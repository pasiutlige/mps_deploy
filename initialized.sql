-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jan 17, 2024 at 10:31 AM
-- Server version: 8.0.31
-- PHP Version: 7.4.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `initialized`
--

-- --------------------------------------------------------

--
-- Table structure for table `address_book`
--

DROP TABLE IF EXISTS `address_book`;
CREATE TABLE IF NOT EXISTS `address_book` (
  `id` int NOT NULL,
  `user_id` int NOT NULL,
  `user_email` varchar(200) NOT NULL,
  `current_times` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

-- --------------------------------------------------------

--
-- Table structure for table `chat_message`
--

DROP TABLE IF EXISTS `chat_message`;
CREATE TABLE IF NOT EXISTS `chat_message` (
  `id` int NOT NULL AUTO_INCREMENT,
  `message` mediumtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `reply_to` int DEFAULT NULL,
  `participant_id` int NOT NULL,
  `room_id` int NOT NULL,
  `datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `room_id` (`room_id`),
  KEY `participant_id` (`participant_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `chat_message_files`
--

DROP TABLE IF EXISTS `chat_message_files`;
CREATE TABLE IF NOT EXISTS `chat_message_files` (
  `id` int NOT NULL AUTO_INCREMENT,
  `file_id` int NOT NULL,
  `message_id` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `message_id` (`message_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `chat_message_status`
--

DROP TABLE IF EXISTS `chat_message_status`;
CREATE TABLE IF NOT EXISTS `chat_message_status` (
  `id` int NOT NULL AUTO_INCREMENT,
  `participant_id` int NOT NULL,
  `message_id` int NOT NULL,
  `status` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'unread',
  PRIMARY KEY (`id`),
  KEY `message_id` (`message_id`),
  KEY `participant_id` (`participant_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `chat_participants`
--

DROP TABLE IF EXISTS `chat_participants`;
CREATE TABLE IF NOT EXISTS `chat_participants` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int NOT NULL,
  `room_id` int NOT NULL,
  `status` tinyint DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `room_id` (`room_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `chat_room`
--

DROP TABLE IF EXISTS `chat_room`;
CREATE TABLE IF NOT EXISTS `chat_room` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `post_id` int DEFAULT NULL,
  `type` tinyint DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `compiled_orders`
--

DROP TABLE IF EXISTS `compiled_orders`;
CREATE TABLE IF NOT EXISTS `compiled_orders` (
  `id` int NOT NULL AUTO_INCREMENT,
  `clientID` int DEFAULT NULL,
  `clientTitle` mediumtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `countryId` int DEFAULT NULL,
  `drawings` mediumtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `employeeId` int DEFAULT NULL,
  `factoryId` int DEFAULT NULL,
  `mainStatus` mediumtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `mounting` mediumtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `onHoldReason` mediumtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `orderCreated` varchar(300) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `orderId` int DEFAULT NULL,
  `orderIdNum` varchar(300) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `orderNum` varchar(300) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `orderOwner` varchar(300) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `orderStatus` varchar(300) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `orderStatusVal` varchar(300) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `orderTitle` varchar(300) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ownerInitials` varchar(300) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `productCount` varchar(300) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shippingAddress` varchar(300) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shippingDate` varchar(300) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shippingNo` varchar(300) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shippingStatus` varchar(300) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shippingStatusVal` varchar(300) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shippingId` varchar(300) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `orderEmployees` varchar(300) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `countries_settings`
--

DROP TABLE IF EXISTS `countries_settings`;
CREATE TABLE IF NOT EXISTS `countries_settings` (
  `id` int NOT NULL AUTO_INCREMENT,
  `country_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `country_abbreviation` varchar(55) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `country_post`
--

DROP TABLE IF EXISTS `country_post`;
CREATE TABLE IF NOT EXISTS `country_post` (
  `id` int NOT NULL AUTO_INCREMENT,
  `post_id` int NOT NULL,
  `country_id` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `country_id` (`country_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `country_user`
--

DROP TABLE IF EXISTS `country_user`;
CREATE TABLE IF NOT EXISTS `country_user` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int NOT NULL,
  `country_id` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `country_id` (`country_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `disable_email_notifications`
--

DROP TABLE IF EXISTS `disable_email_notifications`;
CREATE TABLE IF NOT EXISTS `disable_email_notifications` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int NOT NULL,
  `disable_status` bit(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `disable_email_notifications`
--

INSERT INTO `disable_email_notifications` (`id`, `user_id`, `disable_status`) VALUES
(1, 1, b'0');

-- --------------------------------------------------------

--
-- Table structure for table `disable_post_notifications`
--

DROP TABLE IF EXISTS `disable_post_notifications`;
CREATE TABLE IF NOT EXISTS `disable_post_notifications` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int NOT NULL,
  `post_id` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `disable_status` bit(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `email_draft`
--

DROP TABLE IF EXISTS `email_draft`;
CREATE TABLE IF NOT EXISTS `email_draft` (
  `id` int NOT NULL,
  `user_id` int NOT NULL,
  `user_to` text NOT NULL,
  `user_cc` text NOT NULL,
  `user_bcc` text NOT NULL,
  `user_message` text NOT NULL,
  `user_subject` text NOT NULL,
  `unique_id` text NOT NULL,
  `mail_type` text NOT NULL,
  `file_attachments` text NOT NULL,
  `current_times_ar` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `email_sign`
--

DROP TABLE IF EXISTS `email_sign`;
CREATE TABLE IF NOT EXISTS `email_sign` (
  `id` int NOT NULL,
  `user_id` int NOT NULL,
  `user_name` text NOT NULL,
  `email_sign` text NOT NULL,
  `autorespondtext` text NOT NULL,
  `autorespond_status` int NOT NULL,
  `current_times_ar` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `fabrication_drawings`
--

DROP TABLE IF EXISTS `fabrication_drawings`;
CREATE TABLE IF NOT EXISTS `fabrication_drawings` (
  `id` int NOT NULL AUTO_INCREMENT,
  `author_id` int DEFAULT NULL,
  `order_id` int DEFAULT NULL,
  `file_id` int DEFAULT NULL,
  `file_status` mediumtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `upload_date` mediumtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `product_id` mediumtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `comment` mediumtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `disproved_files` mediumtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `primary_file` bit(1) DEFAULT NULL,
  `improved_on` int DEFAULT NULL,
  `sent_to_client` bit(1) DEFAULT NULL,
  `first_approve_date` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `second_approve_date` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `disapproved_date` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `disapproved_user_id` int DEFAULT NULL,
  `first_approve_user_id` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `second_approve_user_id` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sent_to_client_date` mediumtext COLLATE utf8mb4_unicode_ci,
  `sent_to_client_user` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `factories_settings`
--

DROP TABLE IF EXISTS `factories_settings`;
CREATE TABLE IF NOT EXISTS `factories_settings` (
  `id` int NOT NULL AUTO_INCREMENT,
  `factory_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `factory_name_abbreviation` varchar(55) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `factory_post`
--

DROP TABLE IF EXISTS `factory_post`;
CREATE TABLE IF NOT EXISTS `factory_post` (
  `id` int NOT NULL AUTO_INCREMENT,
  `post_id` int NOT NULL,
  `factory_id` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `factory_id` (`factory_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `factory_user`
--

DROP TABLE IF EXISTS `factory_user`;
CREATE TABLE IF NOT EXISTS `factory_user` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int NOT NULL,
  `factory_id` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `factory_id` (`factory_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `gallery_information`
--

DROP TABLE IF EXISTS `gallery_information`;
CREATE TABLE IF NOT EXISTS `gallery_information` (
  `id` int NOT NULL AUTO_INCREMENT,
  `image_id` bigint DEFAULT NULL,
  `image_description` mediumtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `gallery_tags`
--

DROP TABLE IF EXISTS `gallery_tags`;
CREATE TABLE IF NOT EXISTS `gallery_tags` (
  `id` int NOT NULL AUTO_INCREMENT,
  `tag_name` mediumtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `image_tags`
--

DROP TABLE IF EXISTS `image_tags`;
CREATE TABLE IF NOT EXISTS `image_tags` (
  `id` int NOT NULL AUTO_INCREMENT,
  `image_id` bigint DEFAULT NULL,
  `tag_id` bigint DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `image_urls`
--

DROP TABLE IF EXISTS `image_urls`;
CREATE TABLE IF NOT EXISTS `image_urls` (
  `id` int NOT NULL,
  `image_url` text NOT NULL,
  `image_author` varchar(100) NOT NULL,
  `image_name` text NOT NULL,
  `current_times` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `inbox`
--

DROP TABLE IF EXISTS `inbox`;
CREATE TABLE IF NOT EXISTS `inbox` (
  `id` int NOT NULL,
  `all_data` text NOT NULL,
  `message_id` varchar(100) NOT NULL,
  `is_authenticated` varchar(6) NOT NULL,
  `header_msg_id` varchar(100) NOT NULL,
  `reference` varchar(100) NOT NULL,
  `from_to_concat_id` varchar(100) NOT NULL,
  `subject` varchar(100) NOT NULL,
  `mailcount` int NOT NULL,
  `mail_type` varchar(100) NOT NULL,
  `mail_sender` varchar(100) NOT NULL,
  `mail_receipt` varchar(100) NOT NULL,
  `current_times` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `attachments` text NOT NULL,
  `delete_status` varchar(16) NOT NULL,
  `sent_status` varchar(16) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `mounting_crews`
--

DROP TABLE IF EXISTS `mounting_crews`;
CREATE TABLE IF NOT EXISTS `mounting_crews` (
  `id` int NOT NULL AUTO_INCREMENT,
  `crew_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `crew_abbreviation` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `api_credentials` json DEFAULT NULL,
  `calendar_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `sync` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `color` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '#3f51b5',
  `country` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `mounting_crew_user`
--

DROP TABLE IF EXISTS `mounting_crew_user`;
CREATE TABLE IF NOT EXISTS `mounting_crew_user` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int NOT NULL,
  `crew_id` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `crew_id` (`crew_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `mouting_jobs_files`
--

DROP TABLE IF EXISTS `mouting_jobs_files`;
CREATE TABLE IF NOT EXISTS `mouting_jobs_files` (
  `id` int NOT NULL AUTO_INCREMENT,
  `order_id` int DEFAULT NULL,
  `file_id` int DEFAULT NULL,
  `event_id` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `mrp_image_urls`
--

DROP TABLE IF EXISTS `mrp_image_urls`;
CREATE TABLE IF NOT EXISTS `mrp_image_urls` (
  `id` int NOT NULL,
  `image_url` text NOT NULL,
  `image_author` varchar(100) NOT NULL,
  `image_name` text NOT NULL,
  `current_times` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

-- --------------------------------------------------------

--
-- Table structure for table `mrp_unapproved_posts`
--

DROP TABLE IF EXISTS `mrp_unapproved_posts`;
CREATE TABLE IF NOT EXISTS `mrp_unapproved_posts` (
  `id` int NOT NULL,
  `post_id` int NOT NULL,
  `stage_id` int NOT NULL,
  `current_times` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `new_post` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

DROP TABLE IF EXISTS `notifications`;
CREATE TABLE IF NOT EXISTS `notifications` (
  `id` int NOT NULL AUTO_INCREMENT,
  `post_id` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `datetime` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `notifications_data`
--

DROP TABLE IF EXISTS `notifications_data`;
CREATE TABLE IF NOT EXISTS `notifications_data` (
  `id` int NOT NULL AUTO_INCREMENT,
  `notif_id` int NOT NULL,
  `post_id` int NOT NULL,
  `notif_text` mediumtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `notif_title` mediumtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `user_id` int NOT NULL,
  `post_type` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `field_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `old_data` mediumtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `new_data` mediumtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `datetime` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `notif_id` (`notif_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `notifications_users_data`
--

DROP TABLE IF EXISTS `notifications_users_data`;
CREATE TABLE IF NOT EXISTS `notifications_users_data` (
  `id` int NOT NULL AUTO_INCREMENT,
  `notif_id` int NOT NULL,
  `user_id` int NOT NULL,
  `change_status` bit(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `notif_id` (`notif_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `options_bck`
--

DROP TABLE IF EXISTS `options_bck`;
CREATE TABLE IF NOT EXISTS `options_bck` (
  `option_id` bigint UNSIGNED NOT NULL,
  `option_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `option_value` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `autoload` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'yes'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `orders_linking`
--

DROP TABLE IF EXISTS `orders_linking`;
CREATE TABLE IF NOT EXISTS `orders_linking` (
  `id` int NOT NULL,
  `thread_mail_ids` text NOT NULL,
  `client_id` int NOT NULL,
  `thread_subject` text NOT NULL,
  `thread_order_link_id` text NOT NULL,
  `current_times` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `order_drawing_date`
--

DROP TABLE IF EXISTS `order_drawing_date`;
CREATE TABLE IF NOT EXISTS `order_drawing_date` (
  `id` int NOT NULL AUTO_INCREMENT,
  `order_id` int NOT NULL,
  `is_drawing_enabled` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'false',
  `input_value` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `order_products`
--

DROP TABLE IF EXISTS `order_products`;
CREATE TABLE IF NOT EXISTS `order_products` (
  `id` int NOT NULL AUTO_INCREMENT,
  `unique_id` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order_id` int NOT NULL,
  `product_id` int DEFAULT NULL,
  `input_id` int NOT NULL,
  `input_value` mediumtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `origin_user`
--

DROP TABLE IF EXISTS `origin_user`;
CREATE TABLE IF NOT EXISTS `origin_user` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int NOT NULL,
  `country_id` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `country_id` (`country_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `product_input_table`
--

DROP TABLE IF EXISTS `product_input_table`;
CREATE TABLE IF NOT EXISTS `product_input_table` (
  `id` int NOT NULL AUTO_INCREMENT,
  `product_id` int DEFAULT NULL,
  `input_name` mediumtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `input_type` mediumtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `width` int NOT NULL DEFAULT '200',
  `notify` tinyint(1) NOT NULL DEFAULT '0',
  `input_index` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `product_id` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `product_input_values`
--

DROP TABLE IF EXISTS `product_input_values`;
CREATE TABLE IF NOT EXISTS `product_input_values` (
  `id` int NOT NULL AUTO_INCREMENT,
  `input_id` int NOT NULL,
  `input_value` mediumtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `input_id` (`input_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `product_table`
--

DROP TABLE IF EXISTS `product_table`;
CREATE TABLE IF NOT EXISTS `product_table` (
  `id` int NOT NULL AUTO_INCREMENT,
  `title` mediumtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `img_url` mediumtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `unread_room`
--

DROP TABLE IF EXISTS `unread_room`;
CREATE TABLE IF NOT EXISTS `unread_room` (
  `id` int NOT NULL AUTO_INCREMENT,
  `participant_id` int NOT NULL,
  `room_id` int NOT NULL,
  `datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `version_log`
--

DROP TABLE IF EXISTS `version_log`;
CREATE TABLE IF NOT EXISTS `version_log` (
  `id` int NOT NULL AUTO_INCREMENT,
  `content` mediumtext CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `version` mediumtext CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

--
-- Dumping data for table `version_log`
--

INSERT INTO `version_log` (`id`, `content`, `version`, `created`) VALUES
(1, '[{\"type\":\"paragraph\",\"children\":[{\"text\":\"\"}]}]', '1.0.0', '2024-01-17 10:29:08');

-- --------------------------------------------------------

--
-- Table structure for table `wp_commentmeta`
--

DROP TABLE IF EXISTS `wp_commentmeta`;
CREATE TABLE IF NOT EXISTS `wp_commentmeta` (
  `meta_id` bigint UNSIGNED NOT NULL,
  `comment_id` bigint UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`meta_id`),
  KEY `comment_id` (`comment_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_comments`
--

DROP TABLE IF EXISTS `wp_comments`;
CREATE TABLE IF NOT EXISTS `wp_comments` (
  `comment_ID` bigint UNSIGNED NOT NULL,
  `comment_post_ID` bigint UNSIGNED NOT NULL DEFAULT '0',
  `comment_author` tinytext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment_author_email` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment_karma` int NOT NULL DEFAULT '0',
  `comment_approved` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_type` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'comment',
  `comment_parent` bigint UNSIGNED NOT NULL DEFAULT '0',
  `user_id` bigint UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`comment_ID`),
  KEY `comment_post_ID` (`comment_post_ID`),
  KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  KEY `comment_date_gmt` (`comment_date_gmt`),
  KEY `comment_parent` (`comment_parent`),
  KEY `comment_author_email` (`comment_author_email`(10))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_documentation_files`
--

DROP TABLE IF EXISTS `wp_documentation_files`;
CREATE TABLE IF NOT EXISTS `wp_documentation_files` (
  `id` int NOT NULL AUTO_INCREMENT,
  `order_id` int DEFAULT NULL,
  `file_id` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_dwg_files`
--

DROP TABLE IF EXISTS `wp_dwg_files`;
CREATE TABLE IF NOT EXISTS `wp_dwg_files` (
  `id` int NOT NULL AUTO_INCREMENT,
  `order_id` int DEFAULT NULL,
  `file_id` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_external_designer_files`
--

DROP TABLE IF EXISTS `wp_external_designer_files`;
CREATE TABLE IF NOT EXISTS `wp_external_designer_files` (
  `id` int NOT NULL AUTO_INCREMENT,
  `author_id` int DEFAULT NULL,
  `order_id` int DEFAULT NULL,
  `file_id` int DEFAULT NULL,
  `file_status` mediumtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `upload_date` mediumtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `product_id` mediumtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `comment` mediumtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `disproved_files` mediumtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `primary_file` bit(1) DEFAULT NULL,
  `improved_on` int DEFAULT NULL,
  `sent_to_client` bit(1) DEFAULT NULL,
  `first_approve_date` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `second_approve_date` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `disapproved_date` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `disapproved_user_id` int DEFAULT NULL,
  `first_approve_user_id` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `second_approve_user_id` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sent_to_client_date` mediumtext COLLATE utf8mb4_unicode_ci,
  `sent_to_client_user` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_factory_files`
--

DROP TABLE IF EXISTS `wp_factory_files`;
CREATE TABLE IF NOT EXISTS `wp_factory_files` (
  `id` int NOT NULL AUTO_INCREMENT,
  `author_id` int DEFAULT NULL,
  `order_id` int DEFAULT NULL,
  `file_id` int DEFAULT NULL,
  `file_status` mediumtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `upload_date` mediumtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `product_id` mediumtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `comment` mediumtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `disproved_files` mediumtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `primary_file` bit(1) DEFAULT NULL,
  `improved_on` int DEFAULT NULL,
  `sent_to_client` bit(1) DEFAULT b'0',
  `first_approve_user_id` int DEFAULT NULL,
  `second_approve_user_id` int DEFAULT NULL,
  `first_approve_date` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `second_approve_date` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `disapproved_date` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `disapproved_user_id` int DEFAULT NULL,
  `sent_to_client_date` mediumtext COLLATE utf8mb4_unicode_ci,
  `sent_to_client_user` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_links`
--

DROP TABLE IF EXISTS `wp_links`;
CREATE TABLE IF NOT EXISTS `wp_links` (
  `link_id` bigint UNSIGNED NOT NULL,
  `link_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_image` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_target` varchar(25) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_visible` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Y',
  `link_owner` bigint UNSIGNED NOT NULL DEFAULT '1',
  `link_rating` int NOT NULL DEFAULT '0',
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_notes` mediumtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `link_rss` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`link_id`),
  KEY `link_visible` (`link_visible`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_mgmlp_folders`
--

DROP TABLE IF EXISTS `wp_mgmlp_folders`;
CREATE TABLE IF NOT EXISTS `wp_mgmlp_folders` (
  `post_id` bigint NOT NULL,
  `folder_id` bigint NOT NULL,
  PRIMARY KEY (`post_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb3;

-- --------------------------------------------------------

--
-- Table structure for table `wp_mounting_jobs`
--

DROP TABLE IF EXISTS `wp_mounting_jobs`;
CREATE TABLE IF NOT EXISTS `wp_mounting_jobs` (
  `id` int NOT NULL AUTO_INCREMENT,
  `order_id` int DEFAULT NULL,
  `title` mediumtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `author` int DEFAULT NULL,
  `client` int DEFAULT NULL,
  `region` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `comment` mediumtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `address` mediumtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `start_date` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `end_date` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `google_calendar_event_id` mediumtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `local_id` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_mounting_jobs_assigned`
--

DROP TABLE IF EXISTS `wp_mounting_jobs_assigned`;
CREATE TABLE IF NOT EXISTS `wp_mounting_jobs_assigned` (
  `id` int NOT NULL AUTO_INCREMENT,
  `worker_id` int NOT NULL,
  `job_id` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `job_id` (`job_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_mrp_stages`
--

DROP TABLE IF EXISTS `wp_mrp_stages`;
CREATE TABLE IF NOT EXISTS `wp_mrp_stages` (
  `id` int NOT NULL AUTO_INCREMENT,
  `personnel` varchar(200) DEFAULT NULL,
  `start_date` varchar(200) DEFAULT NULL,
  `end_date` varchar(200) DEFAULT NULL,
  `take_photo` varchar(200) DEFAULT NULL,
  `take_photo_url` varchar(200) DEFAULT NULL,
  `upload_photo` varchar(200) DEFAULT NULL,
  `upload_photo_url` varchar(200) DEFAULT NULL,
  `comment` varchar(200) DEFAULT NULL,
  `product_id` int NOT NULL,
  `stage` varchar(200) NOT NULL,
  `start_time` varchar(200) DEFAULT NULL,
  `end_time` varchar(200) DEFAULT NULL,
  `status` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

-- --------------------------------------------------------

--
-- Table structure for table `wp_mrp_stage_status`
--

DROP TABLE IF EXISTS `wp_mrp_stage_status`;
CREATE TABLE IF NOT EXISTS `wp_mrp_stage_status` (
  `id` int NOT NULL,
  `product_id` int NOT NULL,
  `stagestep` int NOT NULL,
  `current_times` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `stage_title` varchar(200) NOT NULL,
  `status` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `product_id` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

-- --------------------------------------------------------

--
-- Table structure for table `wp_mtrlwid`
--

DROP TABLE IF EXISTS `wp_mtrlwid`;
CREATE TABLE IF NOT EXISTS `wp_mtrlwid` (
  `id` int NOT NULL,
  `session_id` varchar(255) NOT NULL,
  `knp_date` date NOT NULL,
  `knp_time` time NOT NULL,
  `knp_ts` varchar(50) NOT NULL,
  `duration` time NOT NULL,
  `userid` varchar(50) NOT NULL,
  `event` varchar(50) NOT NULL,
  `browser` varchar(50) NOT NULL,
  `platform` varchar(50) NOT NULL,
  `ip` varchar(20) NOT NULL,
  `city` varchar(50) NOT NULL,
  `region` varchar(50) NOT NULL,
  `countryName` varchar(50) NOT NULL,
  `url_id` varchar(255) NOT NULL,
  `url_term` varchar(255) NOT NULL,
  `referer_doamin` varchar(255) NOT NULL,
  `referer_url` text NOT NULL,
  `screensize` varchar(50) NOT NULL,
  `isunique` varchar(50) NOT NULL,
  `landing` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

-- --------------------------------------------------------

--
-- Table structure for table `wp_mtrlwid_online`
--

DROP TABLE IF EXISTS `wp_mtrlwid_online`;
CREATE TABLE IF NOT EXISTS `wp_mtrlwid_online` (
  `id` int NOT NULL,
  `session_id` varchar(255) NOT NULL,
  `knp_time` datetime NOT NULL,
  `knp_ts` varchar(50) NOT NULL,
  `userid` varchar(50) NOT NULL,
  `url_id` varchar(255) NOT NULL,
  `url_term` varchar(255) NOT NULL,
  `city` varchar(50) NOT NULL,
  `region` varchar(50) NOT NULL,
  `countryName` varchar(50) NOT NULL,
  `browser` varchar(50) NOT NULL,
  `platform` varchar(50) NOT NULL,
  `referer_doamin` varchar(255) NOT NULL,
  `referer_url` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

-- --------------------------------------------------------

--
-- Table structure for table `wp_notifications`
--

DROP TABLE IF EXISTS `wp_notifications`;
CREATE TABLE IF NOT EXISTS `wp_notifications` (
  `id` int NOT NULL AUTO_INCREMENT,
  `post_id` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `notif_text` mediumtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `old_data` mediumtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `new_data` mediumtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `user_id` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `post_type` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `field_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `datetime` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `multi` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `notif_title` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `notified` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_notifications_multi`
--

DROP TABLE IF EXISTS `wp_notifications_multi`;
CREATE TABLE IF NOT EXISTS `wp_notifications_multi` (
  `id` int NOT NULL AUTO_INCREMENT,
  `notif_id` int NOT NULL,
  `notif_text` mediumtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `user_id` int NOT NULL,
  `field_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `old_data` mediumtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `new_data` mediumtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `datetime` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `notif_id` (`notif_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_notifications_multi_users`
--

DROP TABLE IF EXISTS `wp_notifications_multi_users`;
CREATE TABLE IF NOT EXISTS `wp_notifications_multi_users` (
  `id` int NOT NULL AUTO_INCREMENT,
  `change_id` int NOT NULL,
  `user_id` int NOT NULL,
  `change_status` bit(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `change_id` (`change_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_notifications_temp`
--

DROP TABLE IF EXISTS `wp_notifications_temp`;
CREATE TABLE IF NOT EXISTS `wp_notifications_temp` (
  `id` int NOT NULL AUTO_INCREMENT,
  `post_id` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `datetime` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `field_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `old_data` mediumtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `new_data` mediumtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_notifications_users`
--

DROP TABLE IF EXISTS `wp_notifications_users`;
CREATE TABLE IF NOT EXISTS `wp_notifications_users` (
  `id` int NOT NULL AUTO_INCREMENT,
  `notif_id` int NOT NULL,
  `user_id` int NOT NULL,
  `notif_status` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `notif_id` (`notif_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_options`
--

DROP TABLE IF EXISTS `wp_options`;
CREATE TABLE IF NOT EXISTS `wp_options` (
  `option_id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `option_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `option_value` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `autoload` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'yes',
  PRIMARY KEY (`option_id`),
  UNIQUE KEY `option_name` (`option_name`),
  KEY `autoload` (`autoload`)
) ENGINE=InnoDB AUTO_INCREMENT=80746 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_options`
--

INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1, 'siteurl', 'http://oscerp.com', 'yes'),
(2, 'home', 'http://oscerp.com', 'yes'),
(3, 'blogname', 'MPS', 'yes'),
(4, 'blogdescription', '', 'yes'),
(5, 'users_can_register', '0', 'yes'),
(6, 'admin_email', 'd,janulis@talacka.com', 'yes'),
(7, 'start_of_week', '1', 'yes'),
(8, 'use_balanceTags', '0', 'yes'),
(9, 'use_smilies', '1', 'yes'),
(10, 'require_name_email', '1', 'yes'),
(11, 'comments_notify', '1', 'yes'),
(12, 'posts_per_rss', '10', 'yes'),
(13, 'rss_use_excerpt', '0', 'yes'),
(14, 'mailserver_url', 'mail.example.com', 'yes'),
(15, 'mailserver_login', 'login@example.com', 'yes'),
(16, 'mailserver_pass', 'password', 'yes'),
(17, 'mailserver_port', '110', 'yes'),
(18, 'default_category', '1', 'yes'),
(19, 'default_comment_status', 'open', 'yes'),
(20, 'default_ping_status', 'open', 'yes'),
(21, 'default_pingback_flag', '1', 'yes'),
(22, 'posts_per_page', '10', 'yes'),
(23, 'date_format', 'F j, Y', 'yes'),
(24, 'time_format', 'g:i a', 'yes'),
(25, 'links_updated_date_format', 'F j, Y g:i a', 'yes'),
(26, 'comment_moderation', '', 'yes'),
(27, 'moderation_notify', '1', 'yes'),
(28, 'permalink_structure', '/%postname%/', 'yes'),
(29, 'rewrite_rules', 'a:208:{s:11:\"^wp-json/?$\";s:22:\"index.php?rest_route=/\";s:14:\"^wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:21:\"^index.php/wp-json/?$\";s:22:\"index.php?rest_route=/\";s:24:\"^index.php/wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:17:\"^wp-sitemap\\.xml$\";s:23:\"index.php?sitemap=index\";s:17:\"^wp-sitemap\\.xsl$\";s:36:\"index.php?sitemap-stylesheet=sitemap\";s:23:\"^wp-sitemap-index\\.xsl$\";s:34:\"index.php?sitemap-stylesheet=index\";s:48:\"^wp-sitemap-([a-z]+?)-([a-z\\d_-]+?)-(\\d+?)\\.xml$\";s:75:\"index.php?sitemap=$matches[1]&sitemap-subtype=$matches[2]&paged=$matches[3]\";s:34:\"^wp-sitemap-([a-z]+?)-(\\d+?)\\.xml$\";s:47:\"index.php?sitemap=$matches[1]&paged=$matches[2]\";s:14:\"all_actions/?$\";s:31:\"index.php?post_type=all_actions\";s:44:\"all_actions/feed/(feed|rdf|rss|rss2|atom)/?$\";s:48:\"index.php?post_type=all_actions&feed=$matches[1]\";s:39:\"all_actions/(feed|rdf|rss|rss2|atom)/?$\";s:48:\"index.php?post_type=all_actions&feed=$matches[1]\";s:31:\"all_actions/page/([0-9]{1,})/?$\";s:49:\"index.php?post_type=all_actions&paged=$matches[1]\";s:19:\"sale_opportunity/?$\";s:36:\"index.php?post_type=sale_opportunity\";s:49:\"sale_opportunity/feed/(feed|rdf|rss|rss2|atom)/?$\";s:53:\"index.php?post_type=sale_opportunity&feed=$matches[1]\";s:44:\"sale_opportunity/(feed|rdf|rss|rss2|atom)/?$\";s:53:\"index.php?post_type=sale_opportunity&feed=$matches[1]\";s:36:\"sale_opportunity/page/([0-9]{1,})/?$\";s:54:\"index.php?post_type=sale_opportunity&paged=$matches[1]\";s:9:\"client/?$\";s:26:\"index.php?post_type=client\";s:39:\"client/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?post_type=client&feed=$matches[1]\";s:34:\"client/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?post_type=client&feed=$matches[1]\";s:26:\"client/page/([0-9]{1,})/?$\";s:44:\"index.php?post_type=client&paged=$matches[1]\";s:17:\"shipping_order/?$\";s:34:\"index.php?post_type=shipping_order\";s:47:\"shipping_order/feed/(feed|rdf|rss|rss2|atom)/?$\";s:51:\"index.php?post_type=shipping_order&feed=$matches[1]\";s:42:\"shipping_order/(feed|rdf|rss|rss2|atom)/?$\";s:51:\"index.php?post_type=shipping_order&feed=$matches[1]\";s:34:\"shipping_order/page/([0-9]{1,})/?$\";s:52:\"index.php?post_type=shipping_order&paged=$matches[1]\";s:16:\"manufacturing/?$\";s:33:\"index.php?post_type=manufacturing\";s:46:\"manufacturing/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_type=manufacturing&feed=$matches[1]\";s:41:\"manufacturing/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_type=manufacturing&feed=$matches[1]\";s:33:\"manufacturing/page/([0-9]{1,})/?$\";s:51:\"index.php?post_type=manufacturing&paged=$matches[1]\";s:47:\"category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:42:\"category/(.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:23:\"category/(.+?)/embed/?$\";s:46:\"index.php?category_name=$matches[1]&embed=true\";s:35:\"category/(.+?)/page/?([0-9]{1,})/?$\";s:53:\"index.php?category_name=$matches[1]&paged=$matches[2]\";s:17:\"category/(.+?)/?$\";s:35:\"index.php?category_name=$matches[1]\";s:44:\"tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:39:\"tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:20:\"tag/([^/]+)/embed/?$\";s:36:\"index.php?tag=$matches[1]&embed=true\";s:32:\"tag/([^/]+)/page/?([0-9]{1,})/?$\";s:43:\"index.php?tag=$matches[1]&paged=$matches[2]\";s:14:\"tag/([^/]+)/?$\";s:25:\"index.php?tag=$matches[1]\";s:45:\"type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:40:\"type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:21:\"type/([^/]+)/embed/?$\";s:44:\"index.php?post_format=$matches[1]&embed=true\";s:33:\"type/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?post_format=$matches[1]&paged=$matches[2]\";s:15:\"type/([^/]+)/?$\";s:33:\"index.php?post_format=$matches[1]\";s:39:\"all_actions/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:49:\"all_actions/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:69:\"all_actions/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:64:\"all_actions/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:64:\"all_actions/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:45:\"all_actions/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:28:\"all_actions/([^/]+)/embed/?$\";s:44:\"index.php?all_actions=$matches[1]&embed=true\";s:32:\"all_actions/([^/]+)/trackback/?$\";s:38:\"index.php?all_actions=$matches[1]&tb=1\";s:52:\"all_actions/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?all_actions=$matches[1]&feed=$matches[2]\";s:47:\"all_actions/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?all_actions=$matches[1]&feed=$matches[2]\";s:40:\"all_actions/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?all_actions=$matches[1]&paged=$matches[2]\";s:47:\"all_actions/([^/]+)/comment-page-([0-9]{1,})/?$\";s:51:\"index.php?all_actions=$matches[1]&cpage=$matches[2]\";s:36:\"all_actions/([^/]+)(?:/([0-9]+))?/?$\";s:50:\"index.php?all_actions=$matches[1]&page=$matches[2]\";s:28:\"all_actions/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:38:\"all_actions/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:58:\"all_actions/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:53:\"all_actions/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:53:\"all_actions/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:34:\"all_actions/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:44:\"sale_opportunity/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:54:\"sale_opportunity/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:74:\"sale_opportunity/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:69:\"sale_opportunity/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:69:\"sale_opportunity/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:50:\"sale_opportunity/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:33:\"sale_opportunity/([^/]+)/embed/?$\";s:49:\"index.php?sale_opportunity=$matches[1]&embed=true\";s:37:\"sale_opportunity/([^/]+)/trackback/?$\";s:43:\"index.php?sale_opportunity=$matches[1]&tb=1\";s:57:\"sale_opportunity/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:55:\"index.php?sale_opportunity=$matches[1]&feed=$matches[2]\";s:52:\"sale_opportunity/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:55:\"index.php?sale_opportunity=$matches[1]&feed=$matches[2]\";s:45:\"sale_opportunity/([^/]+)/page/?([0-9]{1,})/?$\";s:56:\"index.php?sale_opportunity=$matches[1]&paged=$matches[2]\";s:52:\"sale_opportunity/([^/]+)/comment-page-([0-9]{1,})/?$\";s:56:\"index.php?sale_opportunity=$matches[1]&cpage=$matches[2]\";s:41:\"sale_opportunity/([^/]+)(?:/([0-9]+))?/?$\";s:55:\"index.php?sale_opportunity=$matches[1]&page=$matches[2]\";s:33:\"sale_opportunity/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:43:\"sale_opportunity/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:63:\"sale_opportunity/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:58:\"sale_opportunity/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:58:\"sale_opportunity/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:39:\"sale_opportunity/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:34:\"client/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:44:\"client/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:64:\"client/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:59:\"client/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:59:\"client/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:40:\"client/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:23:\"client/([^/]+)/embed/?$\";s:39:\"index.php?client=$matches[1]&embed=true\";s:27:\"client/([^/]+)/trackback/?$\";s:33:\"index.php?client=$matches[1]&tb=1\";s:47:\"client/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:45:\"index.php?client=$matches[1]&feed=$matches[2]\";s:42:\"client/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:45:\"index.php?client=$matches[1]&feed=$matches[2]\";s:35:\"client/([^/]+)/page/?([0-9]{1,})/?$\";s:46:\"index.php?client=$matches[1]&paged=$matches[2]\";s:42:\"client/([^/]+)/comment-page-([0-9]{1,})/?$\";s:46:\"index.php?client=$matches[1]&cpage=$matches[2]\";s:31:\"client/([^/]+)(?:/([0-9]+))?/?$\";s:45:\"index.php?client=$matches[1]&page=$matches[2]\";s:23:\"client/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:33:\"client/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:53:\"client/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:48:\"client/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:48:\"client/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:29:\"client/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:42:\"shipping_order/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:52:\"shipping_order/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:72:\"shipping_order/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:67:\"shipping_order/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:67:\"shipping_order/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:48:\"shipping_order/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:31:\"shipping_order/([^/]+)/embed/?$\";s:47:\"index.php?shipping_order=$matches[1]&embed=true\";s:35:\"shipping_order/([^/]+)/trackback/?$\";s:41:\"index.php?shipping_order=$matches[1]&tb=1\";s:55:\"shipping_order/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:53:\"index.php?shipping_order=$matches[1]&feed=$matches[2]\";s:50:\"shipping_order/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:53:\"index.php?shipping_order=$matches[1]&feed=$matches[2]\";s:43:\"shipping_order/([^/]+)/page/?([0-9]{1,})/?$\";s:54:\"index.php?shipping_order=$matches[1]&paged=$matches[2]\";s:50:\"shipping_order/([^/]+)/comment-page-([0-9]{1,})/?$\";s:54:\"index.php?shipping_order=$matches[1]&cpage=$matches[2]\";s:39:\"shipping_order/([^/]+)(?:/([0-9]+))?/?$\";s:53:\"index.php?shipping_order=$matches[1]&page=$matches[2]\";s:31:\"shipping_order/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:41:\"shipping_order/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:61:\"shipping_order/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:56:\"shipping_order/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:56:\"shipping_order/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:37:\"shipping_order/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:41:\"manufacturing/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:51:\"manufacturing/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:71:\"manufacturing/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:66:\"manufacturing/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:66:\"manufacturing/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:47:\"manufacturing/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:30:\"manufacturing/([^/]+)/embed/?$\";s:46:\"index.php?manufacturing=$matches[1]&embed=true\";s:34:\"manufacturing/([^/]+)/trackback/?$\";s:40:\"index.php?manufacturing=$matches[1]&tb=1\";s:54:\"manufacturing/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?manufacturing=$matches[1]&feed=$matches[2]\";s:49:\"manufacturing/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?manufacturing=$matches[1]&feed=$matches[2]\";s:42:\"manufacturing/([^/]+)/page/?([0-9]{1,})/?$\";s:53:\"index.php?manufacturing=$matches[1]&paged=$matches[2]\";s:49:\"manufacturing/([^/]+)/comment-page-([0-9]{1,})/?$\";s:53:\"index.php?manufacturing=$matches[1]&cpage=$matches[2]\";s:38:\"manufacturing/([^/]+)(?:/([0-9]+))?/?$\";s:52:\"index.php?manufacturing=$matches[1]&page=$matches[2]\";s:30:\"manufacturing/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:40:\"manufacturing/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:60:\"manufacturing/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:55:\"manufacturing/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:55:\"manufacturing/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:36:\"manufacturing/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:12:\"robots\\.txt$\";s:18:\"index.php?robots=1\";s:13:\"favicon\\.ico$\";s:19:\"index.php?favicon=1\";s:48:\".*wp-(atom|rdf|rss|rss2|feed|commentsrss2)\\.php$\";s:18:\"index.php?feed=old\";s:20:\".*wp-app\\.php(/.*)?$\";s:19:\"index.php?error=403\";s:18:\".*wp-register.php$\";s:23:\"index.php?register=true\";s:32:\"feed/(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:27:\"(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:8:\"embed/?$\";s:21:\"index.php?&embed=true\";s:20:\"page/?([0-9]{1,})/?$\";s:28:\"index.php?&paged=$matches[1]\";s:41:\"comments/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:36:\"comments/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:17:\"comments/embed/?$\";s:21:\"index.php?&embed=true\";s:44:\"search/(.+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:39:\"search/(.+)/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:20:\"search/(.+)/embed/?$\";s:34:\"index.php?s=$matches[1]&embed=true\";s:32:\"search/(.+)/page/?([0-9]{1,})/?$\";s:41:\"index.php?s=$matches[1]&paged=$matches[2]\";s:14:\"search/(.+)/?$\";s:23:\"index.php?s=$matches[1]\";s:47:\"author/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:42:\"author/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:23:\"author/([^/]+)/embed/?$\";s:44:\"index.php?author_name=$matches[1]&embed=true\";s:35:\"author/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?author_name=$matches[1]&paged=$matches[2]\";s:17:\"author/([^/]+)/?$\";s:33:\"index.php?author_name=$matches[1]\";s:69:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:64:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:45:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/embed/?$\";s:74:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&embed=true\";s:57:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:81:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&paged=$matches[4]\";s:39:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$\";s:63:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]\";s:56:\"([0-9]{4})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:51:\"([0-9]{4})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:32:\"([0-9]{4})/([0-9]{1,2})/embed/?$\";s:58:\"index.php?year=$matches[1]&monthnum=$matches[2]&embed=true\";s:44:\"([0-9]{4})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:65:\"index.php?year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]\";s:26:\"([0-9]{4})/([0-9]{1,2})/?$\";s:47:\"index.php?year=$matches[1]&monthnum=$matches[2]\";s:43:\"([0-9]{4})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:38:\"([0-9]{4})/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:19:\"([0-9]{4})/embed/?$\";s:37:\"index.php?year=$matches[1]&embed=true\";s:31:\"([0-9]{4})/page/?([0-9]{1,})/?$\";s:44:\"index.php?year=$matches[1]&paged=$matches[2]\";s:13:\"([0-9]{4})/?$\";s:26:\"index.php?year=$matches[1]\";s:27:\".?.+?/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\".?.+?/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\".?.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\".?.+?/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:16:\"(.?.+?)/embed/?$\";s:41:\"index.php?pagename=$matches[1]&embed=true\";s:20:\"(.?.+?)/trackback/?$\";s:35:\"index.php?pagename=$matches[1]&tb=1\";s:40:\"(.?.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:35:\"(.?.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:28:\"(.?.+?)/page/?([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&paged=$matches[2]\";s:35:\"(.?.+?)/comment-page-([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&cpage=$matches[2]\";s:24:\"(.?.+?)(?:/([0-9]+))?/?$\";s:47:\"index.php?pagename=$matches[1]&page=$matches[2]\";s:27:\"[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\"[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\"[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\"[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\"[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\"[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:16:\"([^/]+)/embed/?$\";s:37:\"index.php?name=$matches[1]&embed=true\";s:20:\"([^/]+)/trackback/?$\";s:31:\"index.php?name=$matches[1]&tb=1\";s:40:\"([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?name=$matches[1]&feed=$matches[2]\";s:35:\"([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?name=$matches[1]&feed=$matches[2]\";s:28:\"([^/]+)/page/?([0-9]{1,})/?$\";s:44:\"index.php?name=$matches[1]&paged=$matches[2]\";s:35:\"([^/]+)/comment-page-([0-9]{1,})/?$\";s:44:\"index.php?name=$matches[1]&cpage=$matches[2]\";s:24:\"([^/]+)(?:/([0-9]+))?/?$\";s:43:\"index.php?name=$matches[1]&page=$matches[2]\";s:16:\"[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:26:\"[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:46:\"[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:41:\"[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:41:\"[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:22:\"[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";}', 'yes'),
(30, 'hack_file', '0', 'yes'),
(31, 'blog_charset', 'UTF-8', 'yes'),
(32, 'moderation_keys', '', 'no'),
(33, 'active_plugins', 'a:9:{i:0;s:41:\"acf-to-rest-api/class-acf-to-rest-api.php\";i:1;s:34:\"advanced-custom-fields-pro/acf.php\";i:2;s:48:\"capability-manager-enhanced/capsman-enhanced.php\";i:3;s:39:\"disable-gutenberg/disable-gutenberg.php\";i:5;s:47:\"jwt-authentication-for-wp-rest-api/jwt-auth.php\";i:6;s:28:\"material-admin/mtrl-core.php\";i:7;s:52:\"redux-vendor-support-master/redux-vendor-support.php\";i:9;s:24:\"wp-admin-cache/index.php\";i:10;s:23:\"wp-avatar/wp-avatar.php\";}', 'yes'),
(34, 'category_base', '', 'yes'),
(35, 'ping_sites', 'http://rpc.pingomatic.com/', 'yes'),
(36, 'comment_max_links', '2', 'yes'),
(37, 'gmt_offset', '0', 'yes'),
(38, 'default_email_category', '1', 'yes'),
(39, 'recently_edited', 'a:4:{i:0;s:96:\"/Applications/MAMP/htdocs/metalform/wp-content/plugins/acf-to-rest-api/class-acf-to-rest-api.php\";i:1;s:96:\"/home/wordpress/web/mangird.talacka-labs.com/public_html/wp-content/themes/mangird/functions.php\";i:2;s:92:\"/home/wordpress/web/mangird.talacka-labs.com/public_html/wp-content/themes/mangird/style.css\";i:3;s:0:\"\";}', 'no'),
(40, 'template', 'mangird', 'yes'),
(41, 'stylesheet', 'mangird', 'yes'),
(44, 'comment_registration', '', 'yes'),
(45, 'html_type', 'text/html', 'yes'),
(46, 'use_trackback', '0', 'yes'),
(47, 'default_role', 'manager', 'yes'),
(48, 'db_version', '49752', 'yes'),
(49, 'uploads_use_yearmonth_folders', '1', 'yes'),
(50, 'upload_path', '', 'yes'),
(51, 'blog_public', '1', 'yes'),
(52, 'default_link_category', '2', 'yes'),
(53, 'show_on_front', 'posts', 'yes'),
(54, 'tag_base', '', 'yes'),
(55, 'show_avatars', '1', 'yes'),
(56, 'avatar_rating', 'G', 'yes'),
(57, 'upload_url_path', '', 'yes'),
(58, 'thumbnail_size_w', '150', 'yes'),
(59, 'thumbnail_size_h', '150', 'yes'),
(60, 'thumbnail_crop', '1', 'yes'),
(61, 'medium_size_w', '300', 'yes'),
(62, 'medium_size_h', '300', 'yes'),
(63, 'avatar_default', 'mystery', 'yes'),
(64, 'large_size_w', '1024', 'yes'),
(65, 'large_size_h', '1024', 'yes'),
(66, 'image_default_link_type', 'none', 'yes'),
(67, 'image_default_size', '', 'yes'),
(68, 'image_default_align', '', 'yes'),
(69, 'close_comments_for_old_posts', '', 'yes'),
(70, 'close_comments_days_old', '14', 'yes'),
(71, 'thread_comments', '1', 'yes'),
(72, 'thread_comments_depth', '5', 'yes'),
(73, 'page_comments', '', 'yes'),
(74, 'comments_per_page', '50', 'yes'),
(75, 'default_comments_page', 'newest', 'yes'),
(76, 'comment_order', 'asc', 'yes'),
(77, 'sticky_posts', 'a:0:{}', 'yes'),
(78, 'widget_categories', 'a:2:{i:2;a:4:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:12:\"hierarchical\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(79, 'widget_text', 'a:0:{}', 'yes'),
(80, 'widget_rss', 'a:0:{}', 'yes'),
(81, 'uninstall_plugins', 'a:4:{s:25:\"adminimize/adminimize.php\";s:24:\"_mw_adminimize_uninstall\";s:41:\"better-wp-security/better-wp-security.php\";a:2:{i:0;s:10:\"ITSEC_Core\";i:1;s:16:\"handle_uninstall\";}s:30:\"author-chat/pp-author-chat.php\";s:24:\"pp_author_chat_uninstall\";s:45:\"simple-local-avatars/simple-local-avatars.php\";s:30:\"simple_local_avatars_uninstall\";}', 'no'),
(82, 'timezone_string', '', 'yes'),
(83, 'page_for_posts', '0', 'yes'),
(84, 'page_on_front', '0', 'yes'),
(85, 'default_post_format', '0', 'yes'),
(86, 'link_manager_enabled', '0', 'yes'),
(87, 'finished_splitting_shared_terms', '1', 'yes'),
(88, 'site_icon', '0', 'yes'),
(89, 'medium_large_size_w', '768', 'yes'),
(90, 'medium_large_size_h', '0', 'yes'),
(91, 'wp_page_for_privacy_policy', '3', 'yes'),
(92, 'show_comments_cookies_opt_in', '', 'yes'),
(93, 'initial_db_version', '38590', 'yes'),
(94, 'wp_user_roles', 'a:12:{s:13:\"administrator\";a:2:{s:4:\"name\";s:13:\"Administrator\";s:12:\"capabilities\";a:68:{s:13:\"switch_themes\";b:1;s:11:\"edit_themes\";b:1;s:16:\"activate_plugins\";b:1;s:12:\"edit_plugins\";b:1;s:10:\"edit_users\";b:1;s:10:\"edit_files\";b:1;s:14:\"manage_options\";b:1;s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:6:\"import\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:8:\"level_10\";b:1;s:7:\"level_9\";b:1;s:7:\"level_8\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:12:\"delete_users\";b:1;s:12:\"create_users\";b:1;s:17:\"unfiltered_upload\";b:1;s:14:\"edit_dashboard\";b:1;s:14:\"update_plugins\";b:1;s:14:\"delete_plugins\";b:1;s:15:\"install_plugins\";b:1;s:13:\"update_themes\";b:1;s:14:\"install_themes\";b:1;s:11:\"update_core\";b:1;s:10:\"list_users\";b:1;s:12:\"remove_users\";b:1;s:13:\"promote_users\";b:1;s:18:\"edit_theme_options\";b:1;s:13:\"delete_themes\";b:1;s:6:\"export\";b:1;s:20:\"manage_admin_columns\";b:1;s:19:\"manage_capabilities\";b:1;s:23:\"edit_sale_opportunities\";b:1;s:30:\"edit_others_sale_opportunities\";b:1;s:26:\"publish_sale_opportunities\";b:1;s:33:\"edit_published_sale_opportunities\";b:1;s:31:\"edit_private_sale_opportunities\";b:1;}}s:6:\"author\";a:2:{s:4:\"name\";s:6:\"Author\";s:12:\"capabilities\";a:10:{s:12:\"upload_files\";b:1;s:10:\"edit_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;s:22:\"delete_published_posts\";b:1;}}s:11:\"contributor\";a:2:{s:4:\"name\";s:11:\"Contributor\";s:12:\"capabilities\";a:5:{s:10:\"edit_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;}}s:11:\"super_admin\";a:2:{s:4:\"name\";s:11:\"Super Admin\";s:12:\"capabilities\";a:71:{s:4:\"read\";b:1;s:7:\"level_0\";b:1;s:12:\"edit_clients\";b:1;s:19:\"edit_others_clients\";b:1;s:15:\"publish_clients\";b:1;s:22:\"edit_published_clients\";b:1;s:20:\"edit_private_clients\";b:1;s:23:\"edit_sale_opportunities\";b:1;s:30:\"edit_others_sale_opportunities\";b:1;s:26:\"publish_sale_opportunities\";b:1;s:33:\"edit_published_sale_opportunities\";b:1;s:31:\"edit_private_sale_opportunities\";b:1;s:16:\"edit_all_actions\";b:1;s:23:\"edit_others_all_actions\";b:1;s:19:\"publish_all_actions\";b:1;s:26:\"edit_published_all_actions\";b:1;s:24:\"edit_private_all_actions\";b:1;s:12:\"edit_inboxes\";b:1;s:19:\"edit_others_inboxes\";b:1;s:15:\"publish_inboxes\";b:1;s:22:\"edit_published_inboxes\";b:1;s:20:\"edit_private_inboxes\";b:1;s:19:\"edit_manufacturings\";b:1;s:26:\"edit_others_manufacturings\";b:1;s:22:\"publish_manufacturings\";b:1;s:29:\"edit_published_manufacturings\";b:1;s:27:\"edit_private_manufacturings\";b:1;s:21:\"delete_manufacturings\";b:1;s:28:\"delete_others_manufacturings\";b:1;s:31:\"delete_published_manufacturings\";b:1;s:29:\"delete_private_manufacturings\";b:1;s:12:\"upload_files\";b:1;s:18:\"delete_all_actions\";b:1;s:25:\"delete_others_all_actions\";b:1;s:28:\"delete_published_all_actions\";b:1;s:26:\"delete_private_all_actions\";b:1;s:25:\"delete_sale_opportunities\";b:1;s:32:\"delete_others_sale_opportunities\";b:1;s:35:\"delete_published_sale_opportunities\";b:1;s:33:\"delete_private_sale_opportunities\";b:1;s:14:\"delete_clients\";b:1;s:21:\"delete_others_clients\";b:1;s:24:\"delete_published_clients\";b:1;s:22:\"delete_private_clients\";b:1;s:24:\"read_private_all_actions\";b:1;s:31:\"read_private_sale_opportunities\";b:1;s:20:\"read_private_clients\";b:1;s:9:\"add_users\";b:1;s:12:\"create_users\";b:1;s:10:\"edit_users\";b:1;s:10:\"list_users\";b:1;s:13:\"promote_users\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:13:\"publish_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:10:\"edit_media\";b:1;s:17:\"edit_others_media\";b:1;s:27:\"read_private_manufacturings\";b:1;s:17:\"unfiltered_upload\";b:1;s:20:\"edit_shipping_orders\";b:1;s:27:\"edit_others_shipping_orders\";b:1;s:23:\"publish_shipping_orders\";b:1;s:30:\"edit_published_shipping_orders\";b:1;s:28:\"edit_private_shipping_orders\";b:1;s:22:\"delete_shipping_orders\";b:1;s:29:\"delete_others_shipping_orders\";b:1;s:32:\"delete_published_shipping_orders\";b:1;s:30:\"delete_private_shipping_orders\";b:1;s:28:\"read_private_shipping_orders\";b:1;}}s:6:\"worker\";a:2:{s:4:\"name\";s:6:\"Worker\";s:12:\"capabilities\";a:8:{s:7:\"level_0\";b:1;s:19:\"edit_manufacturings\";b:1;s:12:\"upload_files\";b:1;s:27:\"read_private_manufacturings\";b:1;s:26:\"edit_others_manufacturings\";b:1;s:22:\"publish_manufacturings\";b:1;s:29:\"edit_published_manufacturings\";b:1;s:27:\"edit_private_manufacturings\";b:1;}}s:7:\"manager\";a:2:{s:4:\"name\";s:7:\"Manager\";s:12:\"capabilities\";a:40:{s:10:\"edit_posts\";b:1;s:13:\"publish_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:13:\"edit_products\";b:1;s:20:\"edit_others_products\";b:1;s:16:\"publish_products\";b:1;s:23:\"edit_published_products\";b:1;s:21:\"edit_private_products\";b:1;s:7:\"level_0\";b:1;s:4:\"read\";b:1;s:12:\"upload_files\";b:1;s:17:\"unfiltered_upload\";b:1;s:10:\"edit_files\";b:1;s:16:\"edit_all_actions\";b:1;s:19:\"publish_all_actions\";b:1;s:26:\"publish_sale_opportunities\";b:1;s:12:\"edit_clients\";b:1;s:15:\"publish_clients\";b:1;s:22:\"edit_published_clients\";b:1;s:12:\"delete_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:26:\"edit_published_all_actions\";b:1;s:18:\"delete_all_actions\";b:1;s:28:\"delete_published_all_actions\";b:1;s:35:\"delete_published_sale_opportunities\";b:1;s:14:\"delete_clients\";b:1;s:24:\"delete_published_clients\";b:1;s:18:\"read_private_posts\";b:1;s:18:\"read_private_pages\";b:1;s:24:\"read_private_all_actions\";b:1;s:31:\"read_private_sale_opportunities\";b:1;s:20:\"read_private_clients\";b:1;s:25:\"delete_sale_opportunities\";b:1;s:23:\"edit_sale_opportunities\";b:1;s:33:\"edit_published_sale_opportunities\";b:1;s:19:\"edit_others_clients\";b:1;s:30:\"edit_others_sale_opportunities\";b:1;s:27:\"edit_others_shipping_orders\";b:1;s:30:\"edit_published_shipping_orders\";b:1;}}s:14:\"factory_worker\";a:2:{s:4:\"name\";s:14:\"Factory Worker\";s:12:\"capabilities\";a:17:{s:12:\"upload_files\";b:1;s:23:\"edit_sale_opportunities\";b:1;s:30:\"edit_others_sale_opportunities\";b:1;s:26:\"publish_sale_opportunities\";b:1;s:33:\"edit_published_sale_opportunities\";b:1;s:31:\"edit_private_sale_opportunities\";b:1;s:19:\"edit_manufacturings\";b:1;s:26:\"edit_others_manufacturings\";b:1;s:22:\"publish_manufacturings\";b:1;s:29:\"edit_published_manufacturings\";b:1;s:27:\"edit_private_manufacturings\";b:1;s:31:\"read_private_sale_opportunities\";b:1;s:27:\"read_private_manufacturings\";b:1;s:10:\"edit_files\";b:1;s:4:\"read\";b:1;s:17:\"unfiltered_upload\";b:1;s:7:\"level_0\";b:1;}}s:15:\"mounting_worker\";a:2:{s:4:\"name\";s:15:\"Mounting Worker\";s:12:\"capabilities\";a:12:{s:12:\"upload_files\";b:1;s:23:\"edit_sale_opportunities\";b:1;s:30:\"edit_others_sale_opportunities\";b:1;s:26:\"publish_sale_opportunities\";b:1;s:33:\"edit_published_sale_opportunities\";b:1;s:31:\"edit_private_sale_opportunities\";b:1;s:31:\"read_private_sale_opportunities\";b:1;s:27:\"read_private_manufacturings\";b:1;s:10:\"edit_files\";b:1;s:4:\"read\";b:1;s:17:\"unfiltered_upload\";b:1;s:7:\"level_0\";b:1;}}s:15:\"country_manager\";a:2:{s:4:\"name\";s:15:\"Country Manager\";s:12:\"capabilities\";a:67:{s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:13:\"publish_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:10:\"edit_media\";b:1;s:12:\"upload_files\";b:1;s:17:\"edit_others_media\";b:1;s:16:\"edit_all_actions\";b:1;s:23:\"edit_others_all_actions\";b:1;s:19:\"publish_all_actions\";b:1;s:26:\"edit_published_all_actions\";b:1;s:24:\"edit_private_all_actions\";b:1;s:23:\"edit_sale_opportunities\";b:1;s:30:\"edit_others_sale_opportunities\";b:1;s:26:\"publish_sale_opportunities\";b:1;s:33:\"edit_published_sale_opportunities\";b:1;s:31:\"edit_private_sale_opportunities\";b:1;s:12:\"edit_clients\";b:1;s:19:\"edit_others_clients\";b:1;s:15:\"publish_clients\";b:1;s:22:\"edit_published_clients\";b:1;s:20:\"edit_private_clients\";b:1;s:20:\"edit_shipping_orders\";b:1;s:27:\"edit_others_shipping_orders\";b:1;s:23:\"publish_shipping_orders\";b:1;s:30:\"edit_published_shipping_orders\";b:1;s:28:\"edit_private_shipping_orders\";b:1;s:19:\"edit_manufacturings\";b:1;s:26:\"edit_others_manufacturings\";b:1;s:22:\"publish_manufacturings\";b:1;s:29:\"edit_published_manufacturings\";b:1;s:27:\"edit_private_manufacturings\";b:1;s:18:\"delete_all_actions\";b:1;s:25:\"delete_others_all_actions\";b:1;s:28:\"delete_published_all_actions\";b:1;s:26:\"delete_private_all_actions\";b:1;s:25:\"delete_sale_opportunities\";b:1;s:32:\"delete_others_sale_opportunities\";b:1;s:35:\"delete_published_sale_opportunities\";b:1;s:33:\"delete_private_sale_opportunities\";b:1;s:14:\"delete_clients\";b:1;s:21:\"delete_others_clients\";b:1;s:24:\"delete_published_clients\";b:1;s:22:\"delete_private_clients\";b:1;s:22:\"delete_shipping_orders\";b:1;s:29:\"delete_others_shipping_orders\";b:1;s:32:\"delete_published_shipping_orders\";b:1;s:30:\"delete_private_shipping_orders\";b:1;s:21:\"delete_manufacturings\";b:1;s:28:\"delete_others_manufacturings\";b:1;s:31:\"delete_published_manufacturings\";b:1;s:29:\"delete_private_manufacturings\";b:1;s:24:\"read_private_all_actions\";b:1;s:31:\"read_private_sale_opportunities\";b:1;s:20:\"read_private_clients\";b:1;s:28:\"read_private_shipping_orders\";b:1;s:27:\"read_private_manufacturings\";b:1;s:10:\"list_users\";b:1;s:4:\"read\";b:1;s:17:\"unfiltered_upload\";b:1;s:12:\"edit_inboxes\";b:1;s:19:\"edit_others_inboxes\";b:1;s:20:\"edit_private_inboxes\";b:1;s:22:\"edit_published_inboxes\";b:1;s:15:\"publish_inboxes\";b:1;s:7:\"level_0\";b:1;}}s:8:\"designer\";a:2:{s:4:\"name\";s:8:\"Designer\";s:12:\"capabilities\";a:35:{s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:13:\"publish_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:12:\"upload_files\";b:1;s:16:\"edit_all_actions\";b:1;s:19:\"publish_all_actions\";b:1;s:26:\"edit_published_all_actions\";b:1;s:23:\"edit_sale_opportunities\";b:1;s:30:\"edit_others_sale_opportunities\";b:1;s:26:\"publish_sale_opportunities\";b:1;s:33:\"edit_published_sale_opportunities\";b:1;s:27:\"edit_others_shipping_orders\";b:1;s:30:\"edit_published_shipping_orders\";b:1;s:12:\"delete_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:18:\"delete_all_actions\";b:1;s:28:\"delete_published_all_actions\";b:1;s:25:\"delete_sale_opportunities\";b:1;s:35:\"delete_published_sale_opportunities\";b:1;s:18:\"read_private_posts\";b:1;s:18:\"read_private_pages\";b:1;s:24:\"read_private_all_actions\";b:1;s:31:\"read_private_sale_opportunities\";b:1;s:28:\"read_private_shipping_orders\";b:1;s:10:\"edit_files\";b:1;s:4:\"read\";b:1;s:17:\"unfiltered_upload\";b:1;s:20:\"edit_others_products\";b:1;s:21:\"edit_private_products\";b:1;s:13:\"edit_products\";b:1;s:23:\"edit_published_products\";b:1;s:16:\"publish_products\";b:1;s:7:\"level_0\";b:1;}}s:15:\"factory_manager\";a:2:{s:4:\"name\";s:15:\"Factory Manager\";s:12:\"capabilities\";a:67:{s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:13:\"publish_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:10:\"edit_media\";b:1;s:12:\"upload_files\";b:1;s:17:\"edit_others_media\";b:1;s:16:\"edit_all_actions\";b:1;s:23:\"edit_others_all_actions\";b:1;s:19:\"publish_all_actions\";b:1;s:26:\"edit_published_all_actions\";b:1;s:24:\"edit_private_all_actions\";b:1;s:23:\"edit_sale_opportunities\";b:1;s:30:\"edit_others_sale_opportunities\";b:1;s:26:\"publish_sale_opportunities\";b:1;s:33:\"edit_published_sale_opportunities\";b:1;s:31:\"edit_private_sale_opportunities\";b:1;s:12:\"edit_clients\";b:1;s:19:\"edit_others_clients\";b:1;s:15:\"publish_clients\";b:1;s:22:\"edit_published_clients\";b:1;s:20:\"edit_private_clients\";b:1;s:20:\"edit_shipping_orders\";b:1;s:27:\"edit_others_shipping_orders\";b:1;s:23:\"publish_shipping_orders\";b:1;s:30:\"edit_published_shipping_orders\";b:1;s:28:\"edit_private_shipping_orders\";b:1;s:19:\"edit_manufacturings\";b:1;s:26:\"edit_others_manufacturings\";b:1;s:22:\"publish_manufacturings\";b:1;s:29:\"edit_published_manufacturings\";b:1;s:27:\"edit_private_manufacturings\";b:1;s:18:\"delete_all_actions\";b:1;s:25:\"delete_others_all_actions\";b:1;s:28:\"delete_published_all_actions\";b:1;s:26:\"delete_private_all_actions\";b:1;s:25:\"delete_sale_opportunities\";b:1;s:32:\"delete_others_sale_opportunities\";b:1;s:35:\"delete_published_sale_opportunities\";b:1;s:33:\"delete_private_sale_opportunities\";b:1;s:14:\"delete_clients\";b:1;s:21:\"delete_others_clients\";b:1;s:24:\"delete_published_clients\";b:1;s:22:\"delete_private_clients\";b:1;s:22:\"delete_shipping_orders\";b:1;s:29:\"delete_others_shipping_orders\";b:1;s:32:\"delete_published_shipping_orders\";b:1;s:30:\"delete_private_shipping_orders\";b:1;s:21:\"delete_manufacturings\";b:1;s:28:\"delete_others_manufacturings\";b:1;s:31:\"delete_published_manufacturings\";b:1;s:29:\"delete_private_manufacturings\";b:1;s:24:\"read_private_all_actions\";b:1;s:31:\"read_private_sale_opportunities\";b:1;s:20:\"read_private_clients\";b:1;s:28:\"read_private_shipping_orders\";b:1;s:27:\"read_private_manufacturings\";b:1;s:10:\"list_users\";b:1;s:13:\"promote_users\";b:1;s:4:\"read\";b:1;s:17:\"unfiltered_upload\";b:1;s:12:\"edit_inboxes\";b:1;s:19:\"edit_others_inboxes\";b:1;s:20:\"edit_private_inboxes\";b:1;s:15:\"publish_inboxes\";b:1;s:7:\"level_0\";b:1;}}s:14:\"designer_admin\";a:2:{s:4:\"name\";s:14:\"Designer Admin\";s:12:\"capabilities\";a:67:{s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:13:\"publish_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:10:\"edit_media\";b:1;s:12:\"upload_files\";b:1;s:17:\"edit_others_media\";b:1;s:16:\"edit_all_actions\";b:1;s:23:\"edit_others_all_actions\";b:1;s:19:\"publish_all_actions\";b:1;s:26:\"edit_published_all_actions\";b:1;s:24:\"edit_private_all_actions\";b:1;s:23:\"edit_sale_opportunities\";b:1;s:30:\"edit_others_sale_opportunities\";b:1;s:26:\"publish_sale_opportunities\";b:1;s:33:\"edit_published_sale_opportunities\";b:1;s:31:\"edit_private_sale_opportunities\";b:1;s:12:\"edit_clients\";b:1;s:19:\"edit_others_clients\";b:1;s:15:\"publish_clients\";b:1;s:22:\"edit_published_clients\";b:1;s:20:\"edit_private_clients\";b:1;s:20:\"edit_shipping_orders\";b:1;s:27:\"edit_others_shipping_orders\";b:1;s:23:\"publish_shipping_orders\";b:1;s:30:\"edit_published_shipping_orders\";b:1;s:28:\"edit_private_shipping_orders\";b:1;s:19:\"edit_manufacturings\";b:1;s:26:\"edit_others_manufacturings\";b:1;s:22:\"publish_manufacturings\";b:1;s:29:\"edit_published_manufacturings\";b:1;s:27:\"edit_private_manufacturings\";b:1;s:18:\"delete_all_actions\";b:1;s:25:\"delete_others_all_actions\";b:1;s:28:\"delete_published_all_actions\";b:1;s:26:\"delete_private_all_actions\";b:1;s:25:\"delete_sale_opportunities\";b:1;s:32:\"delete_others_sale_opportunities\";b:1;s:35:\"delete_published_sale_opportunities\";b:1;s:33:\"delete_private_sale_opportunities\";b:1;s:14:\"delete_clients\";b:1;s:21:\"delete_others_clients\";b:1;s:24:\"delete_published_clients\";b:1;s:22:\"delete_private_clients\";b:1;s:22:\"delete_shipping_orders\";b:1;s:29:\"delete_others_shipping_orders\";b:1;s:32:\"delete_published_shipping_orders\";b:1;s:30:\"delete_private_shipping_orders\";b:1;s:21:\"delete_manufacturings\";b:1;s:28:\"delete_others_manufacturings\";b:1;s:31:\"delete_published_manufacturings\";b:1;s:29:\"delete_private_manufacturings\";b:1;s:24:\"read_private_all_actions\";b:1;s:31:\"read_private_sale_opportunities\";b:1;s:20:\"read_private_clients\";b:1;s:28:\"read_private_shipping_orders\";b:1;s:27:\"read_private_manufacturings\";b:1;s:4:\"read\";b:1;s:17:\"unfiltered_upload\";b:1;s:12:\"edit_inboxes\";b:1;s:19:\"edit_others_inboxes\";b:1;s:20:\"edit_private_inboxes\";b:1;s:22:\"edit_published_inboxes\";b:1;s:15:\"publish_inboxes\";b:1;s:7:\"level_0\";b:1;s:10:\"list_users\";b:1;}}}', 'yes'),
(95, 'fresh_site', '0', 'yes'),
(96, 'widget_search', 'a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(97, 'widget_recent-posts', 'a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(98, 'widget_recent-comments', 'a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(99, 'widget_archives', 'a:2:{i:2;a:3:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(100, 'widget_meta', 'a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(101, 'sidebars_widgets', 'a:3:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:13:\"array_version\";i:3;}', 'yes'),
(102, 'widget_pages', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(103, 'widget_calendar', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(104, 'widget_media_audio', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(105, 'widget_media_image', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(106, 'widget_media_gallery', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(107, 'widget_media_video', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(108, 'widget_tag_cloud', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(109, 'widget_nav_menu', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(110, 'widget_custom_html', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(111, 'cron', 'a:10:{i:1705338572;a:1:{s:34:\"wp_privacy_delete_old_export_files\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"hourly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:3600;}}}i:1705343800;a:1:{s:18:\"wp_https_detection\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}i:1705351828;a:1:{s:16:\"new_folder_check\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1705352778;a:1:{s:16:\"wp_version_check\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}i:1705363431;a:2:{s:17:\"wp_update_plugins\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:16:\"wp_update_themes\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}i:1705406644;a:2:{s:19:\"wp_scheduled_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}s:25:\"delete_expired_transients\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1705407194;a:1:{s:30:\"wp_scheduled_auto_draft_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1705412699;a:1:{s:32:\"recovery_mode_clean_expired_keys\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1705646200;a:1:{s:30:\"wp_site_health_scheduled_check\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"weekly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:604800;}}}s:7:\"version\";i:2;}', 'yes'),
(112, 'theme_mods_twentyseventeen', 'a:2:{s:18:\"custom_css_post_id\";i:-1;s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1542024732;s:4:\"data\";a:4:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:9:\"sidebar-2\";a:0:{}s:9:\"sidebar-3\";a:0:{}}}}', 'yes'),
(140, 'recently_activated', 'a:0:{}', 'yes'),
(145, 'acf_version', '5.8.7', 'yes'),
(148, 'current_theme', 'Mangird', 'yes'),
(149, 'theme_mods_mangird', 'a:3:{i:0;b:0;s:18:\"nav_menu_locations\";a:0:{}s:18:\"custom_css_post_id\";i:-1;}', 'yes'),
(150, 'theme_switched', '', 'yes'),
(162, 'auto_core_update_notified', 'a:4:{s:4:\"type\";s:7:\"success\";s:5:\"email\";s:21:\"d,janulis@talacka.com\";s:7:\"version\";s:5:\"5.2.9\";s:9:\"timestamp\";i:1604146367;}', 'no'),
(193, 'category_children', 'a:0:{}', 'yes'),
(214, 'db_upgraded', '', 'yes'),
(328, 'reduk_version_upgraded_from', '3.6.2', 'yes'),
(329, '_transient_timeout__reduk_activation_redirect', '1705487478', 'no'),
(330, '_transient__reduk_activation_redirect', '1', 'no');
INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(339, 'mtrl_demo', 'a:96:{s:8:\"last_tab\";s:0:\"\";s:16:\"dynamic-css-type\";s:6:\"custom\";s:13:\"primary-color\";s:7:\"#3f51b5\";s:7:\"page-bg\";a:7:{s:16:\"background-color\";s:7:\"#EEEEEE\";s:17:\"background-repeat\";s:0:\"\";s:15:\"background-size\";s:0:\"\";s:21:\"background-attachment\";s:0:\"\";s:19:\"background-position\";s:0:\"\";s:16:\"background-image\";s:0:\"\";s:5:\"media\";a:4:{s:2:\"id\";s:0:\"\";s:6:\"height\";s:0:\"\";s:5:\"width\";s:0:\"\";s:9:\"thumbnail\";s:0:\"\";}}s:15:\"page-heading-bg\";a:7:{s:16:\"background-color\";s:7:\"#ffffff\";s:17:\"background-repeat\";s:0:\"\";s:15:\"background-size\";s:0:\"\";s:21:\"background-attachment\";s:0:\"\";s:19:\"background-position\";s:0:\"\";s:16:\"background-image\";s:0:\"\";s:5:\"media\";a:4:{s:2:\"id\";s:0:\"\";s:6:\"height\";s:0:\"\";s:5:\"width\";s:0:\"\";s:9:\"thumbnail\";s:0:\"\";}}s:13:\"heading-color\";s:7:\"#424242\";s:15:\"body-text-color\";s:7:\"#757575\";s:10:\"link-color\";a:2:{s:7:\"regular\";s:7:\"#3f51b5\";s:5:\"hover\";s:7:\"#424242\";}s:11:\"google_body\";a:11:{s:11:\"font-family\";s:6:\"Roboto\";s:12:\"font-options\";s:0:\"\";s:6:\"google\";s:1:\"1\";s:11:\"font-backup\";s:28:\"Arial, Helvetica, sans-serif\";s:11:\"font-weight\";s:3:\"400\";s:10:\"font-style\";s:0:\"\";s:7:\"subsets\";s:0:\"\";s:9:\"font-size\";s:4:\"14px\";s:11:\"line-height\";s:4:\"23px\";s:12:\"word-spacing\";s:0:\"\";s:14:\"letter-spacing\";s:0:\"\";}s:10:\"google_nav\";a:11:{s:11:\"font-family\";s:6:\"Roboto\";s:12:\"font-options\";s:0:\"\";s:6:\"google\";s:1:\"1\";s:11:\"font-backup\";s:28:\"Arial, Helvetica, sans-serif\";s:11:\"font-weight\";s:3:\"300\";s:10:\"font-style\";s:0:\"\";s:7:\"subsets\";s:0:\"\";s:9:\"font-size\";s:4:\"15px\";s:11:\"line-height\";s:4:\"40px\";s:12:\"word-spacing\";s:0:\"\";s:14:\"letter-spacing\";s:0:\"\";}s:15:\"google_headings\";a:9:{s:11:\"font-family\";s:6:\"Roboto\";s:12:\"font-options\";s:0:\"\";s:6:\"google\";s:1:\"1\";s:11:\"font-backup\";s:28:\"Arial, Helvetica, sans-serif\";s:11:\"font-weight\";s:3:\"400\";s:10:\"font-style\";s:0:\"\";s:7:\"subsets\";s:0:\"\";s:12:\"word-spacing\";s:0:\"\";s:14:\"letter-spacing\";s:0:\"\";}s:13:\"google_button\";a:11:{s:11:\"font-family\";s:6:\"Roboto\";s:12:\"font-options\";s:0:\"\";s:6:\"google\";s:1:\"1\";s:11:\"font-backup\";s:28:\"Arial, Helvetica, sans-serif\";s:11:\"font-weight\";s:3:\"400\";s:10:\"font-style\";s:0:\"\";s:7:\"subsets\";s:0:\"\";s:9:\"font-size\";s:4:\"14px\";s:11:\"line-height\";s:4:\"21px\";s:12:\"word-spacing\";s:0:\"\";s:14:\"letter-spacing\";s:0:\"\";}s:10:\"menu-style\";s:6:\"style1\";s:7:\"menu-bg\";a:7:{s:16:\"background-color\";s:7:\"#ffffff\";s:17:\"background-repeat\";s:0:\"\";s:15:\"background-size\";s:0:\"\";s:21:\"background-attachment\";s:0:\"\";s:19:\"background-position\";s:0:\"\";s:16:\"background-image\";s:0:\"\";s:5:\"media\";a:4:{s:2:\"id\";s:0:\"\";s:6:\"height\";s:0:\"\";s:5:\"width\";s:0:\"\";s:9:\"thumbnail\";s:0:\"\";}}s:10:\"menu-color\";s:7:\"#757575\";s:16:\"menu-hover-color\";s:7:\"#212121\";s:13:\"submenu-color\";s:7:\"#757575\";s:15:\"menu-primary-bg\";s:7:\"#eeeeee\";s:17:\"menu-secondary-bg\";s:7:\"#ffffff\";s:19:\"menu-transform-text\";s:10:\"capitalize\";s:11:\"enable-logo\";s:1:\"1\";s:8:\"logo-url\";s:38:\"https://metalform.mangird.com/wp-admin\";s:7:\"logo-bg\";s:11:\"transparent\";s:4:\"logo\";a:5:{s:3:\"url\";s:77:\"https://mps.metalformgroup.com/wp-content/uploads/2019/02/mangird.ai-logo.png\";s:2:\"id\";s:3:\"511\";s:6:\"height\";s:3:\"560\";s:5:\"width\";s:3:\"886\";s:9:\"thumbnail\";s:85:\"https://mps.metalformgroup.com/wp-content/uploads/2019/02/mangird.ai-logo-150x150.png\";}s:11:\"logo_folded\";a:5:{s:3:\"url\";s:0:\"\";s:2:\"id\";s:0:\"\";s:6:\"height\";s:0:\"\";s:5:\"width\";s:0:\"\";s:9:\"thumbnail\";s:0:\"\";}s:10:\"login-logo\";a:5:{s:3:\"url\";s:83:\"https://mps.metalformgroup.com/wp-content/uploads/2019/02/mangird.ai-logo-white.png\";s:2:\"id\";s:3:\"509\";s:6:\"height\";s:3:\"560\";s:5:\"width\";s:3:\"886\";s:9:\"thumbnail\";s:91:\"https://mps.metalformgroup.com/wp-content/uploads/2019/02/mangird.ai-logo-white-150x150.png\";}s:7:\"favicon\";a:5:{s:3:\"url\";s:0:\"\";s:2:\"id\";s:0:\"\";s:6:\"height\";s:0:\"\";s:5:\"width\";s:0:\"\";s:9:\"thumbnail\";s:0:\"\";}s:11:\"iphone_icon\";a:5:{s:3:\"url\";s:0:\"\";s:2:\"id\";s:0:\"\";s:6:\"height\";s:0:\"\";s:5:\"width\";s:0:\"\";s:9:\"thumbnail\";s:0:\"\";}s:18:\"iphone_icon_retina\";a:5:{s:3:\"url\";s:0:\"\";s:2:\"id\";s:0:\"\";s:6:\"height\";s:0:\"\";s:5:\"width\";s:0:\"\";s:9:\"thumbnail\";s:0:\"\";}s:9:\"ipad_icon\";a:5:{s:3:\"url\";s:0:\"\";s:2:\"id\";s:0:\"\";s:6:\"height\";s:0:\"\";s:5:\"width\";s:0:\"\";s:9:\"thumbnail\";s:0:\"\";}s:16:\"ipad_icon_retina\";a:5:{s:3:\"url\";s:0:\"\";s:2:\"id\";s:0:\"\";s:6:\"height\";s:0:\"\";s:5:\"width\";s:0:\"\";s:9:\"thumbnail\";s:0:\"\";}s:6:\"box-bg\";a:7:{s:16:\"background-color\";s:7:\"#ffffff\";s:17:\"background-repeat\";s:0:\"\";s:15:\"background-size\";s:0:\"\";s:21:\"background-attachment\";s:0:\"\";s:19:\"background-position\";s:0:\"\";s:16:\"background-image\";s:0:\"\";s:5:\"media\";a:4:{s:2:\"id\";s:0:\"\";s:6:\"height\";s:0:\"\";s:5:\"width\";s:0:\"\";s:9:\"thumbnail\";s:0:\"\";}}s:11:\"box-head-bg\";a:7:{s:16:\"background-color\";s:7:\"#ffffff\";s:17:\"background-repeat\";s:0:\"\";s:15:\"background-size\";s:0:\"\";s:21:\"background-attachment\";s:0:\"\";s:19:\"background-position\";s:0:\"\";s:16:\"background-image\";s:0:\"\";s:5:\"media\";a:4:{s:2:\"id\";s:0:\"\";s:6:\"height\";s:0:\"\";s:5:\"width\";s:0:\"\";s:9:\"thumbnail\";s:0:\"\";}}s:14:\"box-head-color\";s:7:\"#424242\";s:17:\"button-primary-bg\";s:7:\"#3f51b5\";s:23:\"button-primary-hover-bg\";s:7:\"#E91E63\";s:19:\"button-secondary-bg\";s:7:\"#bdbdbd\";s:25:\"button-secondary-hover-bg\";s:7:\"#9e9e9e\";s:17:\"button-text-color\";s:7:\"#ffffff\";s:7:\"form-bg\";s:11:\"transparent\";s:15:\"form-text-color\";s:7:\"#757575\";s:17:\"form-border-color\";s:7:\"#dedede\";s:16:\"login-background\";a:7:{s:16:\"background-color\";s:7:\"#424242\";s:17:\"background-repeat\";s:0:\"\";s:15:\"background-size\";s:0:\"\";s:21:\"background-attachment\";s:0:\"\";s:19:\"background-position\";s:13:\"center center\";s:16:\"background-image\";s:0:\"\";s:5:\"media\";a:4:{s:2:\"id\";s:0:\"\";s:6:\"height\";s:0:\"\";s:5:\"width\";s:0:\"\";s:9:\"thumbnail\";s:0:\"\";}}s:21:\"login-form-background\";a:7:{s:16:\"background-color\";s:7:\"#424242\";s:17:\"background-repeat\";s:0:\"\";s:15:\"background-size\";s:0:\"\";s:21:\"background-attachment\";s:0:\"\";s:19:\"background-position\";s:0:\"\";s:16:\"background-image\";s:0:\"\";s:5:\"media\";a:4:{s:2:\"id\";s:0:\"\";s:6:\"height\";s:0:\"\";s:5:\"width\";s:0:\"\";s:9:\"thumbnail\";s:0:\"\";}}s:21:\"login-form-bg-opacity\";s:3:\"0.9\";s:21:\"login-logo-background\";a:7:{s:16:\"background-color\";s:7:\"#000000\";s:17:\"background-repeat\";s:0:\"\";s:15:\"background-size\";s:0:\"\";s:21:\"background-attachment\";s:0:\"\";s:19:\"background-position\";s:0:\"\";s:16:\"background-image\";s:0:\"\";s:5:\"media\";a:4:{s:2:\"id\";s:0:\"\";s:6:\"height\";s:0:\"\";s:5:\"width\";s:0:\"\";s:9:\"thumbnail\";s:0:\"\";}}s:16:\"login-text-color\";s:7:\"#9e9e9e\";s:16:\"login-link-color\";a:2:{s:7:\"regular\";s:7:\"#9e9e9e\";s:5:\"hover\";s:7:\"#eeeeee\";}s:22:\"login-input-text-color\";s:7:\"#eeeeee\";s:20:\"login-input-bg-color\";s:11:\"transparent\";s:22:\"login-input-bg-opacity\";s:3:\"0.5\";s:28:\"login-input-bg-hover-opacity\";s:1:\"1\";s:24:\"login-input-border-color\";s:7:\"#9e9e9e\";s:15:\"login-button-bg\";s:7:\"#ffffff\";s:21:\"login-button-hover-bg\";s:7:\"#070707\";s:23:\"login-button-text-color\";s:7:\"#000000\";s:21:\"backtosite_login_link\";s:1:\"1\";s:17:\"forgot_login_link\";s:1:\"1\";s:16:\"login-logo-title\";s:10:\"Login Page\";s:13:\"enable-topbar\";s:1:\"0\";s:16:\"enable-topbar-wp\";s:1:\"0\";s:17:\"topbar-menu-color\";s:7:\"#ffffff\";s:14:\"topbar-menu-bg\";a:7:{s:16:\"background-color\";s:7:\"#3f51b5\";s:17:\"background-repeat\";s:0:\"\";s:15:\"background-size\";s:0:\"\";s:21:\"background-attachment\";s:0:\"\";s:19:\"background-position\";s:0:\"\";s:16:\"background-image\";s:0:\"\";s:5:\"media\";a:4:{s:2:\"id\";s:0:\"\";s:6:\"height\";s:0:\"\";s:5:\"width\";s:0:\"\";s:9:\"thumbnail\";s:0:\"\";}}s:20:\"topbar-submenu-color\";s:7:\"#757575\";s:17:\"topbar-submenu-bg\";s:7:\"#ffffff\";s:23:\"topbar-submenu-hover-bg\";s:7:\"#eeeeee\";s:26:\"topbar-submenu-hover-color\";s:7:\"#757575\";s:22:\"enable-topbar-links-wp\";s:1:\"1\";s:24:\"enable-topbar-links-site\";s:1:\"1\";s:28:\"enable-topbar-links-comments\";s:1:\"1\";s:23:\"enable-topbar-links-new\";s:1:\"1\";s:27:\"enable-topbar-links-updates\";s:1:\"1\";s:29:\"enable-topbar-links-mtrladmin\";s:1:\"1\";s:16:\"topbar-removeids\";s:0:\"\";s:15:\"myaccount_greet\";s:5:\"Howdy\";s:18:\"user-profile-style\";s:6:\"style1\";s:23:\"dashboard-widget-colors\";a:6:{i:0;s:7:\"#7986CB\";i:1;s:7:\"#4dd0e1\";i:2;s:7:\"#9575CD\";i:3;s:7:\"#4FC3F7\";i:4;s:7:\"#64B5F6\";i:5;s:7:\"#4DB6AC\";}s:17:\"dashboard-widgets\";a:11:{s:18:\"mtrl_visitors_type\";s:0:\"\";s:14:\"mtrl_user_type\";s:0:\"\";s:17:\"mtrl_browser_type\";s:0:\"\";s:18:\"mtrl_platform_type\";s:0:\"\";s:17:\"mtrl_country_type\";s:0:\"\";s:19:\"mtrl_today_visitors\";s:0:\"\";s:28:\"mtrl_pagestats_add_dashboard\";s:0:\"\";s:28:\"mtrl_poststats_add_dashboard\";s:0:\"\";s:31:\"mtrl_commentstats_add_dashboard\";s:0:\"\";s:27:\"mtrl_catstats_add_dashboard\";s:0:\"\";s:28:\"mtrl_userstats_add_dashboard\";s:0:\"\";}s:25:\"dashboard-default-widgets\";a:10:{s:13:\"welcome_panel\";s:0:\"\";s:17:\"dashboard_primary\";s:0:\"\";s:21:\"dashboard_quick_press\";s:0:\"\";s:23:\"dashboard_recent_drafts\";s:0:\"\";s:25:\"dashboard_recent_comments\";s:0:\"\";s:19:\"dashboard_right_now\";s:0:\"\";s:18:\"dashboard_activity\";s:0:\"\";s:24:\"dashboard_incoming_links\";s:0:\"\";s:17:\"dashboard_plugins\";s:0:\"\";s:19:\"dashboard_secondary\";s:0:\"\";}s:18:\"front-usertracking\";s:1:\"1\";s:19:\"floatingmenu-enable\";s:1:\"0\";s:18:\"floatingmenu-links\";a:2:{i:0;s:131:\"Add New Client|wp-menu-image dashicons-before dashicons-plus|http://mangird.talacka-labs.com/wp-admin/post-new.php?post_type=client\";i:1;s:126:\"All Clients|wp-menu-image dashicons-before dashicons-id-alt|http://mangird.talacka-labs.com/wp-admin/edit.php?post_type=client\";}s:16:\"floatingmenu-pos\";s:2:\"br\";s:15:\"floatingmenu-bg\";a:7:{s:16:\"background-color\";s:7:\"#e91e63\";s:17:\"background-repeat\";s:0:\"\";s:15:\"background-size\";s:0:\"\";s:21:\"background-attachment\";s:0:\"\";s:19:\"background-position\";s:0:\"\";s:16:\"background-image\";s:0:\"\";s:5:\"media\";a:4:{s:2:\"id\";s:0:\"\";s:6:\"height\";s:0:\"\";s:5:\"width\";s:0:\"\";s:9:\"thumbnail\";s:0:\"\";}}s:18:\"floatingmenu-color\";s:7:\"#ffffff\";s:17:\"floatingmenu-open\";s:5:\"hover\";s:17:\"enable-pageloader\";s:1:\"0\";s:10:\"pace-color\";s:7:\"#e91e63\";s:19:\"enable-smoothscroll\";s:1:\"0\";s:11:\"footer_text\";s:0:\"\";s:14:\"footer_version\";s:1:\"0\";s:17:\"screen_option_tab\";s:1:\"1\";s:15:\"screen_help_tab\";s:1:\"1\";s:10:\"custom-css\";s:188:\"@media only screen and (min-width: 960px) {\r\n    .auto-fold #adminmenuwrap:before {\r\n        background-position: center;\r\n        margin-top: 15px;\r\n        margin-bottom: 15px;\r\n    }\r\n}\";s:17:\"hover3d_translate\";s:1:\"1\";s:14:\"hover3d_shadow\";s:1:\"1\";}', 'yes'),
(340, 'mtrl_demo-transients', 'a:3:{s:14:\"changed_values\";a:2:{s:8:\"last_tab\";s:1:\"1\";s:16:\"login-background\";a:7:{s:16:\"background-color\";s:7:\"#424242\";s:17:\"background-repeat\";s:0:\"\";s:15:\"background-size\";s:0:\"\";s:21:\"background-attachment\";s:0:\"\";s:19:\"background-position\";s:13:\"center center\";s:16:\"background-image\";s:85:\"http://mangird.talacka-labs.com/wp-content/plugins/material-admin/images/login-bg.png\";s:5:\"media\";a:4:{s:2:\"id\";s:0:\"\";s:6:\"height\";s:0:\"\";s:5:\"width\";s:0:\"\";s:9:\"thumbnail\";s:0:\"\";}}}s:9:\"last_save\";i:1642628026;s:13:\"last_compiler\";i:1623913280;}', 'yes'),
(380, 'acf_to_rest_api_request_version', '3', 'yes'),
(673, 'toggle_extra_items', 'a:11:{i:0;s:10:\"upload.php\";i:1;s:17:\"edit-comments.php\";i:2;s:8:\"edit.php\";i:3;s:23:\"edit.php?post_type=page\";i:4;s:10:\"themes.php\";i:5;s:11:\"plugins.php\";i:6;s:9:\"tools.php\";i:7;s:19:\"options-general.php\";i:8;s:34:\"edit.php?post_type=acf-field-group\";i:9;s:12:\"_mtrloptions\";i:10;s:24:\"mtrl_permission_settings\";}', 'yes'),
(738, 'acp_renewal_check', '1563456987', 'no'),
(739, 'ac_version', '3.4.1', 'no'),
(740, 'acp_version', '4.5.5', 'no'),
(741, 'cpac_options_department', 'a:0:{}', 'no'),
(742, 'cpac_options_client__default', 'a:5:{s:2:\"cb\";s:25:\"<input type=\"checkbox\" />\";s:5:\"title\";s:5:\"Title\";s:6:\"author\";s:6:\"Author\";s:4:\"date\";s:4:\"Date\";s:5:\"views\";s:5:\"Views\";}', 'no'),
(743, 'ac_sorting_client_default', 'a:4:{s:5:\"title\";s:5:\"title\";s:6:\"parent\";s:6:\"parent\";s:8:\"comments\";s:13:\"comment_count\";s:4:\"date\";a:2:{i:0;s:4:\"date\";i:1;b:1;}}', 'no'),
(744, 'cpac_options_client', 'a:5:{s:5:\"title\";a:9:{s:4:\"type\";s:5:\"title\";s:5:\"label\";s:5:\"Title\";s:5:\"width\";s:0:\"\";s:10:\"width_unit\";s:1:\"%\";s:4:\"edit\";s:3:\"off\";s:4:\"sort\";s:2:\"on\";s:4:\"name\";s:5:\"title\";s:10:\"label_type\";N;s:6:\"search\";N;}s:13:\"5c7cf1e32e0a2\";a:16:{s:4:\"type\";s:11:\"column-meta\";s:5:\"label\";s:14:\"Company Number\";s:5:\"width\";s:0:\"\";s:10:\"width_unit\";s:1:\"%\";s:5:\"field\";s:14:\"company_number\";s:10:\"field_type\";s:0:\"\";s:6:\"before\";s:0:\"\";s:5:\"after\";s:0:\"\";s:4:\"edit\";s:3:\"off\";s:4:\"sort\";s:2:\"on\";s:6:\"filter\";s:3:\"off\";s:12:\"filter_label\";s:0:\"\";s:4:\"name\";s:13:\"5c7cf1e32e0a2\";s:10:\"label_type\";N;s:13:\"editable_type\";s:8:\"textarea\";s:6:\"search\";N;}s:13:\"5c7cf1e32fba8\";a:16:{s:4:\"type\";s:11:\"column-meta\";s:5:\"label\";s:14:\"Contact Person\";s:5:\"width\";s:0:\"\";s:10:\"width_unit\";s:1:\"%\";s:5:\"field\";s:14:\"contact_person\";s:10:\"field_type\";s:0:\"\";s:6:\"before\";s:0:\"\";s:5:\"after\";s:0:\"\";s:4:\"edit\";s:3:\"off\";s:4:\"sort\";s:2:\"on\";s:6:\"filter\";s:3:\"off\";s:12:\"filter_label\";s:0:\"\";s:4:\"name\";s:13:\"5c7cf1e32fba8\";s:10:\"label_type\";N;s:13:\"editable_type\";s:8:\"textarea\";s:6:\"search\";N;}s:13:\"5c7cf1e32fcb2\";a:16:{s:4:\"type\";s:11:\"column-meta\";s:5:\"label\";s:12:\"Phone Number\";s:5:\"width\";s:0:\"\";s:10:\"width_unit\";s:1:\"%\";s:5:\"field\";s:27:\"contact_person_phone_number\";s:10:\"field_type\";s:0:\"\";s:6:\"before\";s:0:\"\";s:5:\"after\";s:0:\"\";s:4:\"edit\";s:3:\"off\";s:4:\"sort\";s:2:\"on\";s:6:\"filter\";s:3:\"off\";s:12:\"filter_label\";s:0:\"\";s:4:\"name\";s:13:\"5c7cf1e32fcb2\";s:10:\"label_type\";N;s:13:\"editable_type\";s:8:\"textarea\";s:6:\"search\";N;}s:13:\"5c7cf1e32fd22\";a:16:{s:4:\"type\";s:11:\"column-meta\";s:5:\"label\";s:5:\"Email\";s:5:\"width\";s:0:\"\";s:10:\"width_unit\";s:1:\"%\";s:5:\"field\";s:21:\"company_email_address\";s:10:\"field_type\";s:0:\"\";s:6:\"before\";s:0:\"\";s:5:\"after\";s:0:\"\";s:4:\"edit\";s:3:\"off\";s:4:\"sort\";s:2:\"on\";s:6:\"filter\";s:3:\"off\";s:12:\"filter_label\";s:0:\"\";s:4:\"name\";s:13:\"5c7cf1e32fd22\";s:10:\"label_type\";N;s:13:\"editable_type\";s:8:\"textarea\";s:6:\"search\";N;}}', 'no'),
(748, 'cpac_options_sale_opportunity__default', 'a:8:{s:2:\"cb\";s:25:\"<input type=\"checkbox\" />\";s:5:\"title\";s:5:\"Title\";s:11:\"order_price\";s:5:\"Price\";s:6:\"client\";s:6:\"Client\";s:14:\"representative\";s:14:\"Representative\";s:5:\"phone\";s:5:\"Phone\";s:5:\"email\";s:5:\"Email\";s:6:\"status\";s:6:\"Status\";}', 'no'),
(749, 'ac_sorting_sale_opportunity_default', 'a:4:{s:5:\"title\";s:5:\"title\";s:6:\"parent\";s:6:\"parent\";s:8:\"comments\";s:13:\"comment_count\";s:4:\"date\";a:2:{i:0;s:4:\"date\";i:1;b:1;}}', 'no'),
(750, 'cpupdate_cac-pro', 'a21966d6-03b0-43d3-bd44-c8b3bef3df7a', 'no'),
(751, 'cpupdate_cac-pro_sts', 'active', 'no'),
(752, 'cpupdate_cac-pro_expiry_date', '1582324110', 'no'),
(753, 'cpupdate_cac-pro_renewal_discount', '40', 'no'),
(754, 'acpplugin_update_admin-columns-pro', 'O:8:\"stdClass\":6:{s:3:\"url\";s:28:\"https://www.admincolumns.com\";s:4:\"slug\";s:17:\"admin-columns-pro\";s:7:\"package\";b:0;s:11:\"new_version\";s:5:\"4.5.5\";s:2:\"id\";s:1:\"0\";s:5:\"icons\";a:1:{s:3:\"svg\";s:88:\"https://www.admincolumns.com/wp-content/themes/admin-columns/assets/plugin_info/icon.svg\";}}', 'no'),
(755, 'acpplugin_update_admin-columns-pro_timestamp', '1551689813', 'no'),
(759, 'acpplugin_update_ac-addon-acf', 'O:8:\"stdClass\":6:{s:3:\"url\";s:28:\"https://www.admincolumns.com\";s:4:\"slug\";s:12:\"ac-addon-acf\";s:7:\"package\";b:0;s:11:\"new_version\";s:5:\"2.5.1\";s:2:\"id\";s:1:\"0\";s:5:\"icons\";a:1:{s:3:\"svg\";s:88:\"https://www.admincolumns.com/wp-content/themes/admin-columns/assets/plugin_info/icon.svg\";}}', 'no'),
(760, 'acpplugin_update_ac-addon-acf_timestamp', '1551689829', 'no'),
(769, 'cpac_options_post__default', 'a:7:{s:2:\"cb\";s:25:\"<input type=\"checkbox\" />\";s:5:\"title\";s:5:\"Title\";s:6:\"author\";s:6:\"Author\";s:10:\"categories\";s:10:\"Categories\";s:4:\"tags\";s:4:\"Tags\";s:8:\"comments\";s:111:\"<span class=\"vers comment-grey-bubble\" title=\"Comments\"><span class=\"screen-reader-text\">Comments</span></span>\";s:4:\"date\";s:4:\"Date\";}', 'no'),
(770, 'ac_sorting_post_default', 'a:4:{s:5:\"title\";s:5:\"title\";s:6:\"parent\";s:6:\"parent\";s:8:\"comments\";s:13:\"comment_count\";s:4:\"date\";a:2:{i:0;s:4:\"date\";i:1;b:1;}}', 'no'),
(773, 'cpac_options_all_actions__default', 'a:4:{s:2:\"cb\";s:25:\"<input type=\"checkbox\" />\";s:5:\"title\";s:5:\"Title\";s:6:\"author\";s:6:\"Author\";s:4:\"date\";s:4:\"Date\";}', 'no'),
(774, 'ac_sorting_all_actions_default', 'a:4:{s:5:\"title\";s:5:\"title\";s:6:\"parent\";s:6:\"parent\";s:8:\"comments\";s:13:\"comment_count\";s:4:\"date\";a:2:{i:0;s:4:\"date\";i:1;b:1;}}', 'no'),
(1181, 'wp_db_backup_excs', 'a:2:{s:9:\"revisions\";a:1:{i:0;s:8:\"wp_posts\";}s:4:\"spam\";a:1:{i:0;s:11:\"wp_comments\";}}', 'yes'),
(1247, 'WPLANG', '', 'yes'),
(1248, 'new_admin_email', 'jo@metalform.no', 'yes'),
(1249, 'whl_page', 'login', 'yes'),
(1250, 'whl_redirect_admin', '404', 'yes'),
(1253, 'dnh_dismissed_notices', 'a:1:{i:0;s:24:\"wrm_1e278f4992d8bb3f1f0b\";}', 'yes'),
(1261, 'factory_plugin_versions', 'a:1:{s:20:\"wbcr_hide_login_page\";s:10:\"free-1.0.8\";}', 'yes'),
(3805, 'mgmlp_sort_order', '0', 'yes'),
(3806, 'mgmlp_move_or_copy', 'on', 'yes'),
(3807, 'maxgalleria_media_library_version', '5.0.2', 'yes'),
(3808, 'mgmlp_upload_folder_name', 'uploads', 'yes'),
(3809, 'mgmlp_upload_folder_id', '1200', 'yes'),
(3810, 'mgmlp_src_fix', '1', 'yes'),
(3811, 'mlf-folder-check', '2019-07-01 09:48:43', 'yes'),
(3823, 'capsman_version', '2.7.0', 'yes'),
(3824, 'capsman_backup', 'a:6:{s:13:\"administrator\";a:2:{s:4:\"name\";s:13:\"Administrator\";s:12:\"capabilities\";a:62:{s:13:\"switch_themes\";b:1;s:11:\"edit_themes\";b:1;s:16:\"activate_plugins\";b:1;s:12:\"edit_plugins\";b:1;s:10:\"edit_users\";b:1;s:10:\"edit_files\";b:1;s:14:\"manage_options\";b:1;s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:6:\"import\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:8:\"level_10\";b:1;s:7:\"level_9\";b:1;s:7:\"level_8\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:12:\"delete_users\";b:1;s:12:\"create_users\";b:1;s:17:\"unfiltered_upload\";b:1;s:14:\"edit_dashboard\";b:1;s:14:\"update_plugins\";b:1;s:14:\"delete_plugins\";b:1;s:15:\"install_plugins\";b:1;s:13:\"update_themes\";b:1;s:14:\"install_themes\";b:1;s:11:\"update_core\";b:1;s:10:\"list_users\";b:1;s:12:\"remove_users\";b:1;s:13:\"promote_users\";b:1;s:18:\"edit_theme_options\";b:1;s:13:\"delete_themes\";b:1;s:6:\"export\";b:1;s:20:\"manage_admin_columns\";b:1;}}s:6:\"editor\";a:2:{s:4:\"name\";s:6:\"Editor\";s:12:\"capabilities\";a:34:{s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;}}s:6:\"author\";a:2:{s:4:\"name\";s:6:\"Author\";s:12:\"capabilities\";a:10:{s:12:\"upload_files\";b:1;s:10:\"edit_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;s:22:\"delete_published_posts\";b:1;}}s:11:\"contributor\";a:2:{s:4:\"name\";s:11:\"Contributor\";s:12:\"capabilities\";a:5:{s:10:\"edit_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;}}s:10:\"subscriber\";a:2:{s:4:\"name\";s:10:\"Subscriber\";s:12:\"capabilities\";a:2:{s:4:\"read\";b:1;s:7:\"level_0\";b:1;}}s:8:\"employee\";a:2:{s:4:\"name\";s:8:\"Employee\";s:12:\"capabilities\";a:3:{s:4:\"read\";b:1;s:10:\"edit_posts\";b:1;s:12:\"delete_posts\";b:1;}}}', 'no'),
(3825, 'capsman_backup_datestamp', '1561955412', 'no'),
(3826, 'pp_enabled_post_types', 'a:8:{s:7:\"product\";b:1;s:4:\"post\";b:1;s:13:\"manufacturing\";b:1;s:10:\"attachment\";b:1;s:11:\"all_actions\";b:1;s:16:\"sale_opportunity\";b:1;s:6:\"client\";b:1;s:14:\"shipping_order\";b:1;}', 'yes'),
(3827, 'cme_enabled_post_types', 'a:8:{s:7:\"product\";b:1;s:4:\"post\";b:1;s:13:\"manufacturing\";b:1;s:10:\"attachment\";b:1;s:11:\"all_actions\";b:1;s:16:\"sale_opportunity\";b:1;s:6:\"client\";b:1;s:14:\"shipping_order\";b:1;}', 'yes'),
(3828, 'pp_enabled_taxonomies', 'a:0:{}', 'yes'),
(3829, 'cme_detailed_taxonomies', 'a:0:{}', 'yes'),
(4320, 'author_chat_db_version', '1.2', 'yes'),
(4371, 'recovery_keys', 'a:0:{}', 'yes'),
(4385, 'widget_wp_user_avatar_profile', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(4386, 'avatar_default_wp_user_avatar', '1257', 'yes'),
(4387, 'wp_user_avatar_allow_upload', '1', 'yes'),
(4388, 'wp_user_avatar_disable_gravatar', '1', 'yes'),
(4389, 'wp_user_avatar_edit_avatar', '1', 'yes'),
(4390, 'wp_user_avatar_resize_crop', '1', 'yes'),
(4391, 'wp_user_avatar_resize_h', '96', 'yes'),
(4392, 'wp_user_avatar_resize_upload', '1', 'yes'),
(4393, 'wp_user_avatar_resize_w', '96', 'yes'),
(4394, 'wp_user_avatar_tinymce', '1', 'yes'),
(4395, 'wp_user_avatar_upload_size_limit', '8388608', 'yes'),
(4396, 'wp_user_avatar_default_avatar_updated', '1', 'yes'),
(4397, 'wp_user_avatar_users_updated', '1', 'yes'),
(4398, 'wp_user_avatar_media_updated', '1', 'yes'),
(4403, 'wpua_hash_gravatar', 's:142:\"a:2:{s:32:\"774aea4d0f0fd99ce67dca8daec99507\";a:1:{s:10:\"07-11-2019\";b:0;}s:32:\"ad516503a11cd5ca435acc9bb6523536\";a:1:{s:10:\"07-11-2019\";b:1;}}\";', 'yes'),
(4410, 'wp_avatar', 'a:2:{s:18:\"default_avatar_url\";s:0:\"\";s:19:\"allow_anyone_upload\";i:1;}', 'yes'),
(4415, 'recovery_mode_email_last_sent', '1697654216', 'yes'),
(4500, 'heartbeat_control_settings', 'a:3:{s:10:\"rules_dash\";a:1:{i:0;a:2:{s:26:\"heartbeat_control_behavior\";s:5:\"allow\";s:27:\"heartbeat_control_frequency\";s:2:\"15\";}}s:11:\"rules_front\";a:1:{i:0;a:2:{s:26:\"heartbeat_control_behavior\";s:5:\"allow\";s:27:\"heartbeat_control_frequency\";s:2:\"15\";}}s:12:\"rules_editor\";a:1:{i:0;a:2:{s:26:\"heartbeat_control_behavior\";s:5:\"allow\";s:27:\"heartbeat_control_frequency\";s:2:\"15\";}}}', 'yes'),
(4501, 'heartbeat_control_version', '2.0', 'yes'),
(4506, 'mtrladmin_menuorder', 'menu-dashboard|toplevel_page_notifications|toplevel_page_order_list|toplevel_page_confirmed_order_list|toplevel_page_mounting_schedule|toplevel_page_shipping_list|menu-posts|menu-media|menu-pages|menu-comments|menu-posts-all_actions|menu-posts-sale_opportunity|toplevel_page_edit?post_type=sale_opportunity&filter_status=confirmed|menu-posts-client|menu-posts-manufacturing|menu-appearance|menu-plugins|menu-users|menu-tools|menu-settings|toplevel_page_edit?post_type=acf-field-group|toplevel_page__mtrloptions|toplevel_page_mtrl_permission_settings|', 'yes'),
(4507, 'mtrladmin_submenuorder', 'index.php:0|index.php:10|mounting_schedule:0|mounting_schedule:1|edit.php:5|edit.php:10|edit.php:15|edit.php:16|upload.php:5|upload.php:10|edit.php?post_type=page:5|edit.php?post_type=page:10|edit.php?post_type=all_actions:5|edit.php?post_type=all_actions:10|edit.php?post_type=all_actions:11|edit.php?post_type=sale_opportunity:5|edit.php?post_type=sale_opportunity:10|edit.php?post_type=client:5|edit.php?post_type=client:10|edit.php?post_type=manufacturing:5|edit.php?post_type=manufacturing:10|themes.php:5|themes.php:6|themes.php:7|plugins.php:5|plugins.php:10|plugins.php:15|users.php:5|users.php:10|users.php:15|users.php:16|tools.php:5|tools.php:10|tools.php:15|tools.php:20|tools.php:25|tools.php:30|tools.php:37|options-general.php:10|options-general.php:15|options-general.php:20|options-general.php:25|options-general.php:30|options-general.php:40|options-general.php:45|options-general.php:46|options-general.php:47|options-general.php:48|edit.php?post_type=acf-field-group:0|edit.php?post_type=acf-field-group:1|edit.php?post_type=acf-field-group:2|edit.php?post_type=acf-field-group:3|_mtrloptions:1|_mtrloptions:2|_mtrloptions:3|_mtrloptions:4|_mtrloptions:5|_mtrloptions:6|_mtrloptions:7|_mtrloptions:8|_mtrloptions:9|_mtrloptions:10|_mtrloptions:11|_mtrloptions:12|_mtrloptions:13|_mtrloptions:14|_mtrloptions:15|_mtrloptions:16|_mtrloptions:17|mtrl_permission_settings:0|mtrl_permission_settings:1|', 'yes'),
(4508, 'mtrladmin_menurename', '0:menu-dashboard@!@%@Dashboard[$!&!$]dashicons-dashboard|#$%*|2:toplevel_page_notifications@!@%@Notifications[$!&!$]dashicons-schedule|#$%*|3:toplevel_page_order_list@!@%@Orders & Requests[$!&!$]dashicons-admin-post|#$%*|1:toplevel_page_confirmed_order_list@!@%@Confirmed Orders[$!&!$]dashicons-admin-post|#$%*|4:toplevel_page_mounting_schedule@!@%@Mounting Schedule[$!&!$]dashicons-schedule|#$%*|5:toplevel_page_shipping_list@!@%@Shipping List[$!&!$]dashicons-schedule|#$%*|7:menu-posts@!@%@Posts[$!&!$]dashicons-admin-post|#$%*|8:menu-media@!@%@Media[$!&!$]dashicons-admin-media|#$%*|9:menu-pages@!@%@Pages[$!&!$]dashicons-admin-page|#$%*|10:menu-comments@!@%@Comments <span class=\\\"awaiting-mod count-0\\\"><span class=\\\"pending-count\\\" aria-hidden=\\\"true\\\">0</span><span class=\\\"comments-in-moderation-text screen-reader-text\\\">0 Comments in moderation</span></span>[$!&!$]dashicons-admin-comments|#$%*|11:menu-posts-all_actions@!@%@Actions[$!&!$]dashicons-admin-post|#$%*|12:menu-posts-sale_opportunity@!@%@Orders &amp; Requests[$!&!$]dashicons-admin-post|#$%*|13:toplevel_page_edit?post_type=sale_opportunity&filter_status=confirmed@!@%@Confirmed Orders[$!&!$]dashicons-admin-post|#$%*|14:menu-posts-client@!@%@Clients[$!&!$]dashicons-admin-post|#$%*|15:menu-posts-manufacturing@!@%@Manufacturing[$!&!$]dashicons-admin-post|#$%*|17:menu-appearance@!@%@Appearance[$!&!$]dashicons-admin-appearance|#$%*|18:menu-plugins@!@%@Plugins <span class=\\\"update-plugins count-6\\\"><span class=\\\"plugin-count\\\">6</span></span>[$!&!$]dashicons-admin-plugins|#$%*|19:menu-users@!@%@Users[$!&!$]dashicons-admin-users|#$%*|20:menu-tools@!@%@Tools[$!&!$]dashicons-admin-tools|#$%*|21:menu-settings@!@%@Settings[$!&!$]dashicons-admin-settings|#$%*|22:toplevel_page_edit?post_type=acf-field-group@!@%@Custom Fields[$!&!$]dashicons-welcome-widgets-menus|#$%*|24:toplevel_page__mtrloptions@!@%@Material Admin[$!&!$]dashicons-admin-generic|#$%*|25:toplevel_page_mtrl_permission_settings@!@%@Material Admin Addon[$!&!$]dashicons-admin-generic|#$%*|', 'yes'),
(4509, 'mtrladmin_submenurename', 'index.php[($&)]0:0@!@%@Home|#$%*|index.php[($&)]0:10@!@%@Updates <span class=\\\"update-plugins count-7\\\"><span class=\\\"update-count\\\">7</span></span>|#$%*|mounting_schedule[($&)]4:0@!@%@Norway|#$%*|mounting_schedule[($&)]4:1@!@%@United Kingdom|#$%*|edit.php[($&)]7:5@!@%@All Posts|#$%*|edit.php[($&)]7:10@!@%@Add New|#$%*|edit.php[($&)]7:15@!@%@Categories|#$%*|edit.php[($&)]7:16@!@%@Tags|#$%*|upload.php[($&)]8:5@!@%@Library|#$%*|upload.php[($&)]8:10@!@%@Add New|#$%*|edit.php?post_type=page[($&)]9:5@!@%@All Pages|#$%*|edit.php?post_type=page[($&)]9:10@!@%@Add New|#$%*|edit.php?post_type=all_actions[($&)]11:5@!@%@All Actions|#$%*|edit.php?post_type=all_actions[($&)]11:10@!@%@Add New|#$%*|edit.php?post_type=all_actions[($&)]11:11@!@%@Completed Actions|#$%*|edit.php?post_type=sale_opportunity[($&)]12:5@!@%@All Orders|#$%*|edit.php?post_type=sale_opportunity[($&)]12:10@!@%@Add New|#$%*|edit.php?post_type=client[($&)]14:5@!@%@All Clients|#$%*|edit.php?post_type=client[($&)]14:10@!@%@Add New|#$%*|edit.php?post_type=manufacturing[($&)]15:5@!@%@All Manufacturing|#$%*|edit.php?post_type=manufacturing[($&)]15:10@!@%@Add New|#$%*|themes.php[($&)]17:5@!@%@Themes|#$%*|themes.php[($&)]17:6@!@%@Customize|#$%*|themes.php[($&)]17:7@!@%@Theme Editor|#$%*|plugins.php[($&)]18:5@!@%@Installed Plugins|#$%*|plugins.php[($&)]18:10@!@%@Add New|#$%*|plugins.php[($&)]18:15@!@%@Plugin Editor|#$%*|users.php[($&)]19:5@!@%@All Users|#$%*|users.php[($&)]19:10@!@%@Add New|#$%*|users.php[($&)]19:15@!@%@Profile|#$%*|users.php[($&)]19:16@!@%@Capabilities|#$%*|tools.php[($&)]20:5@!@%@Available Tools|#$%*|tools.php[($&)]20:10@!@%@Import|#$%*|tools.php[($&)]20:15@!@%@Export|#$%*|tools.php[($&)]20:20@!@%@Site Health|#$%*|tools.php[($&)]20:25@!@%@Export Personal Data|#$%*|tools.php[($&)]20:30@!@%@Erase Personal Data|#$%*|tools.php[($&)]20:37@!@%@Capability Manager|#$%*|options-general.php[($&)]21:10@!@%@General|#$%*|options-general.php[($&)]21:15@!@%@Writing|#$%*|options-general.php[($&)]21:20@!@%@Reading|#$%*|options-general.php[($&)]21:25@!@%@Discussion|#$%*|options-general.php[($&)]21:30@!@%@Media|#$%*|options-general.php[($&)]21:40@!@%@Permalinks|#$%*|options-general.php[($&)]21:45@!@%@Privacy|#$%*|options-general.php[($&)]21:46@!@%@Disable Gutenberg|#$%*|options-general.php[($&)]21:47@!@%@WP Admin Cache|#$%*|options-general.php[($&)]21:48@!@%@Heartbeat Control Settings|#$%*|edit.php?post_type=acf-field-group[($&)]22:0@!@%@Field Groups|#$%*|edit.php?post_type=acf-field-group[($&)]22:1@!@%@Add New|#$%*|edit.php?post_type=acf-field-group[($&)]22:2@!@%@Tools|#$%*|edit.php?post_type=acf-field-group[($&)]22:3@!@%@Updates|#$%*|_mtrloptions[($&)]24:1@!@%@Pick Theme|#$%*|_mtrloptions[($&)]24:2@!@%@Layout|#$%*|_mtrloptions[($&)]24:3@!@%@Typography Fonts|#$%*|_mtrloptions[($&)]24:4@!@%@Admin Menu|#$%*|_mtrloptions[($&)]24:5@!@%@Logo Settings|#$%*|_mtrloptions[($&)]24:6@!@%@Content Box|#$%*|_mtrloptions[($&)]24:7@!@%@Button|#$%*|_mtrloptions[($&)]24:8@!@%@Form|#$%*|_mtrloptions[($&)]24:9@!@%@Login Page|#$%*|_mtrloptions[($&)]24:10@!@%@Admin Top Bar|#$%*|_mtrloptions[($&)]24:11@!@%@User Profile|#$%*|_mtrloptions[($&)]24:12@!@%@Dashboard Widgets|#$%*|_mtrloptions[($&)]24:13@!@%@Floating Menu|#$%*|_mtrloptions[($&)]24:14@!@%@Page Loader|#$%*|_mtrloptions[($&)]24:15@!@%@Smooth Scroll|#$%*|_mtrloptions[($&)]24:16@!@%@Extra Settings|#$%*|_mtrloptions[($&)]24:17@!@%@Import / Export|#$%*|mtrl_permission_settings[($&)]25:0@!@%@Plugin Settings|#$%*|mtrl_permission_settings[($&)]25:1@!@%@Menu Management|#$%*|', 'yes'),
(4510, 'mtrladmin_menudisable', 'menu-posts|menu-media|menu-pages|menu-comments|menu-posts-sale_opportunity|toplevel_page_edit?post_type=sale_opportunity&filter_status=confirmed|menu-appearance|menu-plugins|menu-tools|menu-settings|toplevel_page_edit?post_type=acf-field-group|toplevel_page__mtrloptions|toplevel_page_mtrl_permission_settings|', 'yes'),
(4511, 'mtrladmin_submenudisable', 'index.php:0|index.php:10|edit.php?post_type=all_actions:10|edit.php?post_type=sale_opportunity:10|edit.php?post_type=client:10|', 'yes'),
(4512, 'mtrladmin_plugin_access', 'specific_user', 'yes'),
(4513, 'mtrladmin_plugin_page', 'show', 'yes'),
(4514, 'mtrladmin_plugin_userid', '1', 'yes'),
(4515, 'mtrladmin_menumng_page', 'enable', 'yes'),
(4516, 'mtrladmin_admin_menumng_page', 'disable', 'yes'),
(4517, 'mtrladmin_admintheme_page', 'enable', 'yes'),
(4518, 'mtrladmin_logintheme_page', 'enable', 'yes'),
(4519, 'mtrladmin_master_theme', '0', 'yes'),
(5004, 'September 3, 2019, 9:05 am_5158', 'order_end_date', 'no'),
(5005, '_September 3, 2019, 9:05 am_5158', '', 'no'),
(5693, '_transient_health-check-site-status-result', '{\"good\":13,\"recommended\":6,\"critical\":1}', 'yes'),
(7036, '_site_transient_update_themes', 'O:8:\"stdClass\":5:{s:12:\"last_checked\";i:1705320232;s:7:\"checked\";a:1:{s:7:\"mangird\";s:0:\"\";}s:8:\"response\";a:0:{}s:9:\"no_update\";a:0:{}s:12:\"translations\";a:0:{}}', 'no'),
(7112, 'adminhash', 'a:2:{s:4:\"hash\";s:32:\"11ac4fa554f96c8837c9d72663d25012\";s:8:\"newemail\";s:15:\"jo@metalform.no\";}', 'yes'),
(8453, 'custom_order_id', '959', 'yes'),
(10146, 'custom_shipping_number_no', '54', 'yes'),
(13425, 'custom_shipping_number_uk', '35', 'yes'),
(23747, 'admin_email_lifespan', '1697823994', 'yes'),
(23748, 'disallowed_keys', '', 'no'),
(23749, 'comment_previously_approved', '1', 'yes'),
(23750, 'auto_plugin_theme_update_emails', 'a:0:{}', 'no'),
(23751, 'auto_update_core_dev', 'enabled', 'yes'),
(23752, 'auto_update_core_minor', 'enabled', 'yes'),
(23753, 'auto_update_core_major', 'unset', 'yes'),
(23754, 'finished_updating_comment_type', '1', 'yes'),
(23755, '_site_transient_update_core', 'O:8:\"stdClass\":4:{s:7:\"updates\";a:9:{i:0;O:8:\"stdClass\":10:{s:8:\"response\";s:7:\"upgrade\";s:8:\"download\";s:59:\"https://downloads.wordpress.org/release/wordpress-6.4.2.zip\";s:6:\"locale\";s:5:\"en_US\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:59:\"https://downloads.wordpress.org/release/wordpress-6.4.2.zip\";s:10:\"no_content\";s:70:\"https://downloads.wordpress.org/release/wordpress-6.4.2-no-content.zip\";s:11:\"new_bundled\";s:71:\"https://downloads.wordpress.org/release/wordpress-6.4.2-new-bundled.zip\";s:7:\"partial\";s:0:\"\";s:8:\"rollback\";s:0:\"\";}s:7:\"current\";s:5:\"6.4.2\";s:7:\"version\";s:5:\"6.4.2\";s:11:\"php_version\";s:5:\"7.0.0\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"6.4\";s:15:\"partial_version\";s:0:\"\";}i:1;O:8:\"stdClass\":11:{s:8:\"response\";s:10:\"autoupdate\";s:8:\"download\";s:59:\"https://downloads.wordpress.org/release/wordpress-6.4.2.zip\";s:6:\"locale\";s:5:\"en_US\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:59:\"https://downloads.wordpress.org/release/wordpress-6.4.2.zip\";s:10:\"no_content\";s:70:\"https://downloads.wordpress.org/release/wordpress-6.4.2-no-content.zip\";s:11:\"new_bundled\";s:71:\"https://downloads.wordpress.org/release/wordpress-6.4.2-new-bundled.zip\";s:7:\"partial\";s:0:\"\";s:8:\"rollback\";s:0:\"\";}s:7:\"current\";s:5:\"6.4.2\";s:7:\"version\";s:5:\"6.4.2\";s:11:\"php_version\";s:5:\"7.0.0\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"6.4\";s:15:\"partial_version\";s:0:\"\";s:9:\"new_files\";s:1:\"1\";}i:2;O:8:\"stdClass\":11:{s:8:\"response\";s:10:\"autoupdate\";s:8:\"download\";s:59:\"https://downloads.wordpress.org/release/wordpress-6.3.2.zip\";s:6:\"locale\";s:5:\"en_US\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:59:\"https://downloads.wordpress.org/release/wordpress-6.3.2.zip\";s:10:\"no_content\";s:70:\"https://downloads.wordpress.org/release/wordpress-6.3.2-no-content.zip\";s:11:\"new_bundled\";s:71:\"https://downloads.wordpress.org/release/wordpress-6.3.2-new-bundled.zip\";s:7:\"partial\";s:0:\"\";s:8:\"rollback\";s:0:\"\";}s:7:\"current\";s:5:\"6.3.2\";s:7:\"version\";s:5:\"6.3.2\";s:11:\"php_version\";s:5:\"7.0.0\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"6.4\";s:15:\"partial_version\";s:0:\"\";s:9:\"new_files\";s:1:\"1\";}i:3;O:8:\"stdClass\":11:{s:8:\"response\";s:10:\"autoupdate\";s:8:\"download\";s:59:\"https://downloads.wordpress.org/release/wordpress-6.2.3.zip\";s:6:\"locale\";s:5:\"en_US\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:59:\"https://downloads.wordpress.org/release/wordpress-6.2.3.zip\";s:10:\"no_content\";s:70:\"https://downloads.wordpress.org/release/wordpress-6.2.3-no-content.zip\";s:11:\"new_bundled\";s:71:\"https://downloads.wordpress.org/release/wordpress-6.2.3-new-bundled.zip\";s:7:\"partial\";s:0:\"\";s:8:\"rollback\";s:0:\"\";}s:7:\"current\";s:5:\"6.2.3\";s:7:\"version\";s:5:\"6.2.3\";s:11:\"php_version\";s:6:\"5.6.20\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"6.4\";s:15:\"partial_version\";s:0:\"\";s:9:\"new_files\";s:1:\"1\";}i:4;O:8:\"stdClass\":11:{s:8:\"response\";s:10:\"autoupdate\";s:8:\"download\";s:59:\"https://downloads.wordpress.org/release/wordpress-6.1.4.zip\";s:6:\"locale\";s:5:\"en_US\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:59:\"https://downloads.wordpress.org/release/wordpress-6.1.4.zip\";s:10:\"no_content\";s:70:\"https://downloads.wordpress.org/release/wordpress-6.1.4-no-content.zip\";s:11:\"new_bundled\";s:71:\"https://downloads.wordpress.org/release/wordpress-6.1.4-new-bundled.zip\";s:7:\"partial\";s:0:\"\";s:8:\"rollback\";s:0:\"\";}s:7:\"current\";s:5:\"6.1.4\";s:7:\"version\";s:5:\"6.1.4\";s:11:\"php_version\";s:6:\"5.6.20\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"6.4\";s:15:\"partial_version\";s:0:\"\";s:9:\"new_files\";s:1:\"1\";}i:5;O:8:\"stdClass\":11:{s:8:\"response\";s:10:\"autoupdate\";s:8:\"download\";s:59:\"https://downloads.wordpress.org/release/wordpress-6.0.6.zip\";s:6:\"locale\";s:5:\"en_US\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:59:\"https://downloads.wordpress.org/release/wordpress-6.0.6.zip\";s:10:\"no_content\";s:70:\"https://downloads.wordpress.org/release/wordpress-6.0.6-no-content.zip\";s:11:\"new_bundled\";s:71:\"https://downloads.wordpress.org/release/wordpress-6.0.6-new-bundled.zip\";s:7:\"partial\";s:0:\"\";s:8:\"rollback\";s:0:\"\";}s:7:\"current\";s:5:\"6.0.6\";s:7:\"version\";s:5:\"6.0.6\";s:11:\"php_version\";s:6:\"5.6.20\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"6.4\";s:15:\"partial_version\";s:0:\"\";s:9:\"new_files\";s:1:\"1\";}i:6;O:8:\"stdClass\":11:{s:8:\"response\";s:10:\"autoupdate\";s:8:\"download\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.9.8.zip\";s:6:\"locale\";s:5:\"en_US\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.9.8.zip\";s:10:\"no_content\";s:70:\"https://downloads.wordpress.org/release/wordpress-5.9.8-no-content.zip\";s:11:\"new_bundled\";s:71:\"https://downloads.wordpress.org/release/wordpress-5.9.8-new-bundled.zip\";s:7:\"partial\";s:0:\"\";s:8:\"rollback\";s:0:\"\";}s:7:\"current\";s:5:\"5.9.8\";s:7:\"version\";s:5:\"5.9.8\";s:11:\"php_version\";s:6:\"5.6.20\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"6.4\";s:15:\"partial_version\";s:0:\"\";s:9:\"new_files\";s:1:\"1\";}i:7;O:8:\"stdClass\":11:{s:8:\"response\";s:10:\"autoupdate\";s:8:\"download\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.8.8.zip\";s:6:\"locale\";s:5:\"en_US\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.8.8.zip\";s:10:\"no_content\";s:70:\"https://downloads.wordpress.org/release/wordpress-5.8.8-no-content.zip\";s:11:\"new_bundled\";s:71:\"https://downloads.wordpress.org/release/wordpress-5.8.8-new-bundled.zip\";s:7:\"partial\";s:0:\"\";s:8:\"rollback\";s:0:\"\";}s:7:\"current\";s:5:\"5.8.8\";s:7:\"version\";s:5:\"5.8.8\";s:11:\"php_version\";s:6:\"5.6.20\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"6.4\";s:15:\"partial_version\";s:0:\"\";s:9:\"new_files\";s:1:\"1\";}i:8;O:8:\"stdClass\":11:{s:8:\"response\";s:10:\"autoupdate\";s:8:\"download\";s:60:\"https://downloads.wordpress.org/release/wordpress-5.7.10.zip\";s:6:\"locale\";s:5:\"en_US\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:60:\"https://downloads.wordpress.org/release/wordpress-5.7.10.zip\";s:10:\"no_content\";s:71:\"https://downloads.wordpress.org/release/wordpress-5.7.10-no-content.zip\";s:11:\"new_bundled\";s:72:\"https://downloads.wordpress.org/release/wordpress-5.7.10-new-bundled.zip\";s:7:\"partial\";s:70:\"https://downloads.wordpress.org/release/wordpress-5.7.10-partial-1.zip\";s:8:\"rollback\";s:71:\"https://downloads.wordpress.org/release/wordpress-5.7.10-rollback-1.zip\";}s:7:\"current\";s:6:\"5.7.10\";s:7:\"version\";s:6:\"5.7.10\";s:11:\"php_version\";s:6:\"5.6.20\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"6.4\";s:15:\"partial_version\";s:5:\"5.7.1\";s:9:\"new_files\";s:0:\"\";}}s:12:\"last_checked\";i:1705309580;s:15:\"version_checked\";s:5:\"5.7.1\";s:12:\"translations\";a:0:{}}', 'no'),
(23764, 'can_compress_scripts', '0', 'no'),
(23771, 'https_detection_errors', 'a:1:{s:19:\"bad_response_source\";a:1:{i:0;s:55:\"It looks like the response did not come from this site.\";}}', 'yes'),
(27962, 'custom_shipping_number_MLF', '213', 'yes'),
(28313, 'custom_shipping_number_WAS', '3', 'yes'),
(28455, 'custom_shipping_number_PMK', '9', 'yes'),
(38762, 'custom_shipping_number_MLF_UK', '173', 'yes'),
(38764, 'custom_shipping_number_PMK_UK', '12', 'yes'),
(38768, 'custom_shipping_number_MLF_NO', '286', 'yes'),
(38788, 'custom_shipping_number_WAS_UK', '2', 'yes'),
(38828, 'custom_shipping_number_PMK_NO', '2', 'yes'),
(38830, 'custom_shipping_number_WAS_NO', '2', 'yes'),
(43436, 'custom_shipping_number_MLF_DE', '9', 'yes'),
(64414, 'custom_shipping_number_SCH_NO', '2', 'yes'),
(70046, 'custom_shipping_number_SEC_NO', '1', 'yes'),
(76176, 'is_script_executed', '1', 'yes'),
(76177, 'is_script_executed_drawings', '1', 'yes'),
(76178, 'products_migrated', '1', 'yes');
INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(76184, 'cme_backup_auto_2023-10-18_7-54-13_pm', 'a:12:{s:13:\"administrator\";a:2:{s:4:\"name\";s:13:\"Administrator\";s:12:\"capabilities\";a:68:{s:13:\"switch_themes\";b:1;s:11:\"edit_themes\";b:1;s:16:\"activate_plugins\";b:1;s:12:\"edit_plugins\";b:1;s:10:\"edit_users\";b:1;s:10:\"edit_files\";b:1;s:14:\"manage_options\";b:1;s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:6:\"import\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:8:\"level_10\";b:1;s:7:\"level_9\";b:1;s:7:\"level_8\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:12:\"delete_users\";b:1;s:12:\"create_users\";b:1;s:17:\"unfiltered_upload\";b:1;s:14:\"edit_dashboard\";b:1;s:14:\"update_plugins\";b:1;s:14:\"delete_plugins\";b:1;s:15:\"install_plugins\";b:1;s:13:\"update_themes\";b:1;s:14:\"install_themes\";b:1;s:11:\"update_core\";b:1;s:10:\"list_users\";b:1;s:12:\"remove_users\";b:1;s:13:\"promote_users\";b:1;s:18:\"edit_theme_options\";b:1;s:13:\"delete_themes\";b:1;s:6:\"export\";b:1;s:20:\"manage_admin_columns\";b:1;s:19:\"manage_capabilities\";b:1;s:23:\"edit_sale_opportunities\";b:1;s:30:\"edit_others_sale_opportunities\";b:1;s:26:\"publish_sale_opportunities\";b:1;s:33:\"edit_published_sale_opportunities\";b:1;s:31:\"edit_private_sale_opportunities\";b:1;}}s:6:\"author\";a:2:{s:4:\"name\";s:6:\"Author\";s:12:\"capabilities\";a:10:{s:12:\"upload_files\";b:1;s:10:\"edit_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;s:22:\"delete_published_posts\";b:1;}}s:11:\"contributor\";a:2:{s:4:\"name\";s:11:\"Contributor\";s:12:\"capabilities\";a:5:{s:10:\"edit_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;}}s:11:\"super_admin\";a:2:{s:4:\"name\";s:11:\"Super Admin\";s:12:\"capabilities\";a:71:{s:4:\"read\";b:1;s:7:\"level_0\";b:1;s:12:\"edit_clients\";b:1;s:19:\"edit_others_clients\";b:1;s:15:\"publish_clients\";b:1;s:22:\"edit_published_clients\";b:1;s:20:\"edit_private_clients\";b:1;s:23:\"edit_sale_opportunities\";b:1;s:30:\"edit_others_sale_opportunities\";b:1;s:26:\"publish_sale_opportunities\";b:1;s:33:\"edit_published_sale_opportunities\";b:1;s:31:\"edit_private_sale_opportunities\";b:1;s:16:\"edit_all_actions\";b:1;s:23:\"edit_others_all_actions\";b:1;s:19:\"publish_all_actions\";b:1;s:26:\"edit_published_all_actions\";b:1;s:24:\"edit_private_all_actions\";b:1;s:12:\"edit_inboxes\";b:1;s:19:\"edit_others_inboxes\";b:1;s:15:\"publish_inboxes\";b:1;s:22:\"edit_published_inboxes\";b:1;s:20:\"edit_private_inboxes\";b:1;s:19:\"edit_manufacturings\";b:1;s:26:\"edit_others_manufacturings\";b:1;s:22:\"publish_manufacturings\";b:1;s:29:\"edit_published_manufacturings\";b:1;s:27:\"edit_private_manufacturings\";b:1;s:21:\"delete_manufacturings\";b:1;s:28:\"delete_others_manufacturings\";b:1;s:31:\"delete_published_manufacturings\";b:1;s:29:\"delete_private_manufacturings\";b:1;s:12:\"upload_files\";b:1;s:18:\"delete_all_actions\";b:1;s:25:\"delete_others_all_actions\";b:1;s:28:\"delete_published_all_actions\";b:1;s:26:\"delete_private_all_actions\";b:1;s:25:\"delete_sale_opportunities\";b:1;s:32:\"delete_others_sale_opportunities\";b:1;s:35:\"delete_published_sale_opportunities\";b:1;s:33:\"delete_private_sale_opportunities\";b:1;s:14:\"delete_clients\";b:1;s:21:\"delete_others_clients\";b:1;s:24:\"delete_published_clients\";b:1;s:22:\"delete_private_clients\";b:1;s:24:\"read_private_all_actions\";b:1;s:31:\"read_private_sale_opportunities\";b:1;s:20:\"read_private_clients\";b:1;s:9:\"add_users\";b:1;s:12:\"create_users\";b:1;s:10:\"edit_users\";b:1;s:10:\"list_users\";b:1;s:13:\"promote_users\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:13:\"publish_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:10:\"edit_media\";b:1;s:17:\"edit_others_media\";b:1;s:27:\"read_private_manufacturings\";b:1;s:17:\"unfiltered_upload\";b:1;s:20:\"edit_shipping_orders\";b:1;s:27:\"edit_others_shipping_orders\";b:1;s:23:\"publish_shipping_orders\";b:1;s:30:\"edit_published_shipping_orders\";b:1;s:28:\"edit_private_shipping_orders\";b:1;s:22:\"delete_shipping_orders\";b:1;s:29:\"delete_others_shipping_orders\";b:1;s:32:\"delete_published_shipping_orders\";b:1;s:30:\"delete_private_shipping_orders\";b:1;s:28:\"read_private_shipping_orders\";b:1;}}s:6:\"worker\";a:2:{s:4:\"name\";s:6:\"Worker\";s:12:\"capabilities\";a:8:{s:7:\"level_0\";b:1;s:19:\"edit_manufacturings\";b:1;s:12:\"upload_files\";b:1;s:27:\"read_private_manufacturings\";b:1;s:26:\"edit_others_manufacturings\";b:1;s:22:\"publish_manufacturings\";b:1;s:29:\"edit_published_manufacturings\";b:1;s:27:\"edit_private_manufacturings\";b:1;}}s:7:\"manager\";a:2:{s:4:\"name\";s:7:\"Manager\";s:12:\"capabilities\";a:40:{s:10:\"edit_posts\";b:1;s:13:\"publish_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:13:\"edit_products\";b:1;s:20:\"edit_others_products\";b:1;s:16:\"publish_products\";b:1;s:23:\"edit_published_products\";b:1;s:21:\"edit_private_products\";b:1;s:7:\"level_0\";b:1;s:4:\"read\";b:1;s:12:\"upload_files\";b:1;s:17:\"unfiltered_upload\";b:1;s:10:\"edit_files\";b:1;s:16:\"edit_all_actions\";b:1;s:19:\"publish_all_actions\";b:1;s:26:\"publish_sale_opportunities\";b:1;s:12:\"edit_clients\";b:1;s:15:\"publish_clients\";b:1;s:22:\"edit_published_clients\";b:1;s:12:\"delete_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:26:\"edit_published_all_actions\";b:1;s:18:\"delete_all_actions\";b:1;s:28:\"delete_published_all_actions\";b:1;s:35:\"delete_published_sale_opportunities\";b:1;s:14:\"delete_clients\";b:1;s:24:\"delete_published_clients\";b:1;s:18:\"read_private_posts\";b:1;s:18:\"read_private_pages\";b:1;s:24:\"read_private_all_actions\";b:1;s:31:\"read_private_sale_opportunities\";b:1;s:20:\"read_private_clients\";b:1;s:25:\"delete_sale_opportunities\";b:1;s:23:\"edit_sale_opportunities\";b:1;s:33:\"edit_published_sale_opportunities\";b:1;s:19:\"edit_others_clients\";b:1;s:30:\"edit_others_sale_opportunities\";b:1;s:27:\"edit_others_shipping_orders\";b:1;s:30:\"edit_published_shipping_orders\";b:1;}}s:14:\"factory_worker\";a:2:{s:4:\"name\";s:14:\"Factory Worker\";s:12:\"capabilities\";a:17:{s:12:\"upload_files\";b:1;s:23:\"edit_sale_opportunities\";b:1;s:30:\"edit_others_sale_opportunities\";b:1;s:26:\"publish_sale_opportunities\";b:1;s:33:\"edit_published_sale_opportunities\";b:1;s:31:\"edit_private_sale_opportunities\";b:1;s:19:\"edit_manufacturings\";b:1;s:26:\"edit_others_manufacturings\";b:1;s:22:\"publish_manufacturings\";b:1;s:29:\"edit_published_manufacturings\";b:1;s:27:\"edit_private_manufacturings\";b:1;s:31:\"read_private_sale_opportunities\";b:1;s:27:\"read_private_manufacturings\";b:1;s:10:\"edit_files\";b:1;s:4:\"read\";b:1;s:17:\"unfiltered_upload\";b:1;s:7:\"level_0\";b:1;}}s:15:\"mounting_worker\";a:2:{s:4:\"name\";s:15:\"Mounting Worker\";s:12:\"capabilities\";a:12:{s:12:\"upload_files\";b:1;s:23:\"edit_sale_opportunities\";b:1;s:30:\"edit_others_sale_opportunities\";b:1;s:26:\"publish_sale_opportunities\";b:1;s:33:\"edit_published_sale_opportunities\";b:1;s:31:\"edit_private_sale_opportunities\";b:1;s:31:\"read_private_sale_opportunities\";b:1;s:27:\"read_private_manufacturings\";b:1;s:10:\"edit_files\";b:1;s:4:\"read\";b:1;s:17:\"unfiltered_upload\";b:1;s:7:\"level_0\";b:1;}}s:15:\"country_manager\";a:2:{s:4:\"name\";s:15:\"Country Manager\";s:12:\"capabilities\";a:67:{s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:13:\"publish_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:10:\"edit_media\";b:1;s:12:\"upload_files\";b:1;s:17:\"edit_others_media\";b:1;s:16:\"edit_all_actions\";b:1;s:23:\"edit_others_all_actions\";b:1;s:19:\"publish_all_actions\";b:1;s:26:\"edit_published_all_actions\";b:1;s:24:\"edit_private_all_actions\";b:1;s:23:\"edit_sale_opportunities\";b:1;s:30:\"edit_others_sale_opportunities\";b:1;s:26:\"publish_sale_opportunities\";b:1;s:33:\"edit_published_sale_opportunities\";b:1;s:31:\"edit_private_sale_opportunities\";b:1;s:12:\"edit_clients\";b:1;s:19:\"edit_others_clients\";b:1;s:15:\"publish_clients\";b:1;s:22:\"edit_published_clients\";b:1;s:20:\"edit_private_clients\";b:1;s:20:\"edit_shipping_orders\";b:1;s:27:\"edit_others_shipping_orders\";b:1;s:23:\"publish_shipping_orders\";b:1;s:30:\"edit_published_shipping_orders\";b:1;s:28:\"edit_private_shipping_orders\";b:1;s:19:\"edit_manufacturings\";b:1;s:26:\"edit_others_manufacturings\";b:1;s:22:\"publish_manufacturings\";b:1;s:29:\"edit_published_manufacturings\";b:1;s:27:\"edit_private_manufacturings\";b:1;s:18:\"delete_all_actions\";b:1;s:25:\"delete_others_all_actions\";b:1;s:28:\"delete_published_all_actions\";b:1;s:26:\"delete_private_all_actions\";b:1;s:25:\"delete_sale_opportunities\";b:1;s:32:\"delete_others_sale_opportunities\";b:1;s:35:\"delete_published_sale_opportunities\";b:1;s:33:\"delete_private_sale_opportunities\";b:1;s:14:\"delete_clients\";b:1;s:21:\"delete_others_clients\";b:1;s:24:\"delete_published_clients\";b:1;s:22:\"delete_private_clients\";b:1;s:22:\"delete_shipping_orders\";b:1;s:29:\"delete_others_shipping_orders\";b:1;s:32:\"delete_published_shipping_orders\";b:1;s:30:\"delete_private_shipping_orders\";b:1;s:21:\"delete_manufacturings\";b:1;s:28:\"delete_others_manufacturings\";b:1;s:31:\"delete_published_manufacturings\";b:1;s:29:\"delete_private_manufacturings\";b:1;s:24:\"read_private_all_actions\";b:1;s:31:\"read_private_sale_opportunities\";b:1;s:20:\"read_private_clients\";b:1;s:28:\"read_private_shipping_orders\";b:1;s:27:\"read_private_manufacturings\";b:1;s:10:\"list_users\";b:1;s:4:\"read\";b:1;s:17:\"unfiltered_upload\";b:1;s:12:\"edit_inboxes\";b:1;s:19:\"edit_others_inboxes\";b:1;s:20:\"edit_private_inboxes\";b:1;s:22:\"edit_published_inboxes\";b:1;s:15:\"publish_inboxes\";b:1;s:7:\"level_0\";b:1;}}s:8:\"designer\";a:2:{s:4:\"name\";s:8:\"Designer\";s:12:\"capabilities\";a:35:{s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:13:\"publish_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:12:\"upload_files\";b:1;s:16:\"edit_all_actions\";b:1;s:19:\"publish_all_actions\";b:1;s:26:\"edit_published_all_actions\";b:1;s:23:\"edit_sale_opportunities\";b:1;s:30:\"edit_others_sale_opportunities\";b:1;s:26:\"publish_sale_opportunities\";b:1;s:33:\"edit_published_sale_opportunities\";b:1;s:27:\"edit_others_shipping_orders\";b:1;s:30:\"edit_published_shipping_orders\";b:1;s:12:\"delete_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:18:\"delete_all_actions\";b:1;s:28:\"delete_published_all_actions\";b:1;s:25:\"delete_sale_opportunities\";b:1;s:35:\"delete_published_sale_opportunities\";b:1;s:18:\"read_private_posts\";b:1;s:18:\"read_private_pages\";b:1;s:24:\"read_private_all_actions\";b:1;s:31:\"read_private_sale_opportunities\";b:1;s:28:\"read_private_shipping_orders\";b:1;s:10:\"edit_files\";b:1;s:4:\"read\";b:1;s:17:\"unfiltered_upload\";b:1;s:20:\"edit_others_products\";b:1;s:21:\"edit_private_products\";b:1;s:13:\"edit_products\";b:1;s:23:\"edit_published_products\";b:1;s:16:\"publish_products\";b:1;s:7:\"level_0\";b:1;}}s:15:\"factory_manager\";a:2:{s:4:\"name\";s:15:\"Factory Manager\";s:12:\"capabilities\";a:67:{s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:13:\"publish_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:10:\"edit_media\";b:1;s:12:\"upload_files\";b:1;s:17:\"edit_others_media\";b:1;s:16:\"edit_all_actions\";b:1;s:23:\"edit_others_all_actions\";b:1;s:19:\"publish_all_actions\";b:1;s:26:\"edit_published_all_actions\";b:1;s:24:\"edit_private_all_actions\";b:1;s:23:\"edit_sale_opportunities\";b:1;s:30:\"edit_others_sale_opportunities\";b:1;s:26:\"publish_sale_opportunities\";b:1;s:33:\"edit_published_sale_opportunities\";b:1;s:31:\"edit_private_sale_opportunities\";b:1;s:12:\"edit_clients\";b:1;s:19:\"edit_others_clients\";b:1;s:15:\"publish_clients\";b:1;s:22:\"edit_published_clients\";b:1;s:20:\"edit_private_clients\";b:1;s:20:\"edit_shipping_orders\";b:1;s:27:\"edit_others_shipping_orders\";b:1;s:23:\"publish_shipping_orders\";b:1;s:30:\"edit_published_shipping_orders\";b:1;s:28:\"edit_private_shipping_orders\";b:1;s:19:\"edit_manufacturings\";b:1;s:26:\"edit_others_manufacturings\";b:1;s:22:\"publish_manufacturings\";b:1;s:29:\"edit_published_manufacturings\";b:1;s:27:\"edit_private_manufacturings\";b:1;s:18:\"delete_all_actions\";b:1;s:25:\"delete_others_all_actions\";b:1;s:28:\"delete_published_all_actions\";b:1;s:26:\"delete_private_all_actions\";b:1;s:25:\"delete_sale_opportunities\";b:1;s:32:\"delete_others_sale_opportunities\";b:1;s:35:\"delete_published_sale_opportunities\";b:1;s:33:\"delete_private_sale_opportunities\";b:1;s:14:\"delete_clients\";b:1;s:21:\"delete_others_clients\";b:1;s:24:\"delete_published_clients\";b:1;s:22:\"delete_private_clients\";b:1;s:22:\"delete_shipping_orders\";b:1;s:29:\"delete_others_shipping_orders\";b:1;s:32:\"delete_published_shipping_orders\";b:1;s:30:\"delete_private_shipping_orders\";b:1;s:21:\"delete_manufacturings\";b:1;s:28:\"delete_others_manufacturings\";b:1;s:31:\"delete_published_manufacturings\";b:1;s:29:\"delete_private_manufacturings\";b:1;s:24:\"read_private_all_actions\";b:1;s:31:\"read_private_sale_opportunities\";b:1;s:20:\"read_private_clients\";b:1;s:28:\"read_private_shipping_orders\";b:1;s:27:\"read_private_manufacturings\";b:1;s:10:\"list_users\";b:1;s:13:\"promote_users\";b:1;s:4:\"read\";b:1;s:17:\"unfiltered_upload\";b:1;s:12:\"edit_inboxes\";b:1;s:19:\"edit_others_inboxes\";b:1;s:20:\"edit_private_inboxes\";b:1;s:15:\"publish_inboxes\";b:1;s:7:\"level_0\";b:1;}}s:14:\"designer_admin\";a:2:{s:4:\"name\";s:14:\"Designer Admin\";s:12:\"capabilities\";a:67:{s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:13:\"publish_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:10:\"edit_media\";b:1;s:12:\"upload_files\";b:1;s:17:\"edit_others_media\";b:1;s:16:\"edit_all_actions\";b:1;s:23:\"edit_others_all_actions\";b:1;s:19:\"publish_all_actions\";b:1;s:26:\"edit_published_all_actions\";b:1;s:24:\"edit_private_all_actions\";b:1;s:23:\"edit_sale_opportunities\";b:1;s:30:\"edit_others_sale_opportunities\";b:1;s:26:\"publish_sale_opportunities\";b:1;s:33:\"edit_published_sale_opportunities\";b:1;s:31:\"edit_private_sale_opportunities\";b:1;s:12:\"edit_clients\";b:1;s:19:\"edit_others_clients\";b:1;s:15:\"publish_clients\";b:1;s:22:\"edit_published_clients\";b:1;s:20:\"edit_private_clients\";b:1;s:20:\"edit_shipping_orders\";b:1;s:27:\"edit_others_shipping_orders\";b:1;s:23:\"publish_shipping_orders\";b:1;s:30:\"edit_published_shipping_orders\";b:1;s:28:\"edit_private_shipping_orders\";b:1;s:19:\"edit_manufacturings\";b:1;s:26:\"edit_others_manufacturings\";b:1;s:22:\"publish_manufacturings\";b:1;s:29:\"edit_published_manufacturings\";b:1;s:27:\"edit_private_manufacturings\";b:1;s:18:\"delete_all_actions\";b:1;s:25:\"delete_others_all_actions\";b:1;s:28:\"delete_published_all_actions\";b:1;s:26:\"delete_private_all_actions\";b:1;s:25:\"delete_sale_opportunities\";b:1;s:32:\"delete_others_sale_opportunities\";b:1;s:35:\"delete_published_sale_opportunities\";b:1;s:33:\"delete_private_sale_opportunities\";b:1;s:14:\"delete_clients\";b:1;s:21:\"delete_others_clients\";b:1;s:24:\"delete_published_clients\";b:1;s:22:\"delete_private_clients\";b:1;s:22:\"delete_shipping_orders\";b:1;s:29:\"delete_others_shipping_orders\";b:1;s:32:\"delete_published_shipping_orders\";b:1;s:30:\"delete_private_shipping_orders\";b:1;s:21:\"delete_manufacturings\";b:1;s:28:\"delete_others_manufacturings\";b:1;s:31:\"delete_published_manufacturings\";b:1;s:29:\"delete_private_manufacturings\";b:1;s:24:\"read_private_all_actions\";b:1;s:31:\"read_private_sale_opportunities\";b:1;s:20:\"read_private_clients\";b:1;s:28:\"read_private_shipping_orders\";b:1;s:27:\"read_private_manufacturings\";b:1;s:4:\"read\";b:1;s:17:\"unfiltered_upload\";b:1;s:12:\"edit_inboxes\";b:1;s:19:\"edit_others_inboxes\";b:1;s:20:\"edit_private_inboxes\";b:1;s:22:\"edit_published_inboxes\";b:1;s:15:\"publish_inboxes\";b:1;s:7:\"level_0\";b:1;s:10:\"list_users\";b:1;}}}', 'no'),
(76185, 'capability-manager-enhanced_wp_reviews_installed_on', '2023-10-18 19:54:13', 'yes'),
(77460, 'image_id_migration_completed', '1', 'yes'),
(79187, 'custom_shipping_number_INT_NO', '1', 'yes'),
(80723, '_transient_timeout_acf_plugin_updates', '1705482380', 'no'),
(80724, '_transient_acf_plugin_updates', 'a:5:{s:7:\"plugins\";a:0:{}s:9:\"no_update\";a:1:{s:34:\"advanced-custom-fields-pro/acf.php\";a:12:{s:4:\"slug\";s:26:\"advanced-custom-fields-pro\";s:6:\"plugin\";s:34:\"advanced-custom-fields-pro/acf.php\";s:11:\"new_version\";s:5:\"6.2.4\";s:3:\"url\";s:36:\"https://www.advancedcustomfields.com\";s:6:\"tested\";s:5:\"6.4.2\";s:7:\"package\";s:0:\"\";s:5:\"icons\";a:1:{s:7:\"default\";s:63:\"https://ps.w.org/advanced-custom-fields/assets/icon-256x256.png\";}s:7:\"banners\";a:2:{s:3:\"low\";s:77:\"https://ps.w.org/advanced-custom-fields/assets/banner-772x250.jpg?rev=1729102\";s:4:\"high\";s:78:\"https://ps.w.org/advanced-custom-fields/assets/banner-1544x500.jpg?rev=1729099\";}s:8:\"requires\";s:3:\"5.8\";s:12:\"requires_php\";s:3:\"7.0\";s:12:\"release_date\";s:8:\"20231128\";s:6:\"reason\";s:17:\"wp_not_compatible\";}}s:10:\"expiration\";i:172800;s:6:\"status\";i:1;s:7:\"checked\";a:1:{s:34:\"advanced-custom-fields-pro/acf.php\";s:5:\"5.8.7\";}}', 'no'),
(80727, '_site_transient_update_plugins', 'O:8:\"stdClass\":4:{s:12:\"last_checked\";i:1705320231;s:8:\"response\";a:6:{s:51:\"all-in-one-wp-migration/all-in-one-wp-migration.php\";O:8:\"stdClass\":13:{s:2:\"id\";s:37:\"w.org/plugins/all-in-one-wp-migration\";s:4:\"slug\";s:23:\"all-in-one-wp-migration\";s:6:\"plugin\";s:51:\"all-in-one-wp-migration/all-in-one-wp-migration.php\";s:11:\"new_version\";s:4:\"7.79\";s:3:\"url\";s:54:\"https://wordpress.org/plugins/all-in-one-wp-migration/\";s:7:\"package\";s:71:\"https://downloads.wordpress.org/plugin/all-in-one-wp-migration.7.79.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:76:\"https://ps.w.org/all-in-one-wp-migration/assets/icon-256x256.png?rev=2458334\";s:2:\"1x\";s:76:\"https://ps.w.org/all-in-one-wp-migration/assets/icon-128x128.png?rev=2458334\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:79:\"https://ps.w.org/all-in-one-wp-migration/assets/banner-1544x500.png?rev=2990457\";s:2:\"1x\";s:78:\"https://ps.w.org/all-in-one-wp-migration/assets/banner-772x250.png?rev=2990457\";}s:11:\"banners_rtl\";a:0:{}s:8:\"requires\";s:3:\"3.3\";s:6:\"tested\";s:5:\"6.4.2\";s:12:\"requires_php\";s:3:\"5.3\";s:13:\"compatibility\";O:8:\"stdClass\":0:{}}s:39:\"disable-gutenberg/disable-gutenberg.php\";O:8:\"stdClass\":13:{s:2:\"id\";s:31:\"w.org/plugins/disable-gutenberg\";s:4:\"slug\";s:17:\"disable-gutenberg\";s:6:\"plugin\";s:39:\"disable-gutenberg/disable-gutenberg.php\";s:11:\"new_version\";s:3:\"3.1\";s:3:\"url\";s:48:\"https://wordpress.org/plugins/disable-gutenberg/\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/plugin/disable-gutenberg.3.1.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:70:\"https://ps.w.org/disable-gutenberg/assets/icon-256x256.png?rev=1925990\";s:2:\"1x\";s:70:\"https://ps.w.org/disable-gutenberg/assets/icon-128x128.png?rev=1925990\";}s:7:\"banners\";a:0:{}s:11:\"banners_rtl\";a:0:{}s:8:\"requires\";s:3:\"4.9\";s:6:\"tested\";s:5:\"6.4.2\";s:12:\"requires_php\";s:6:\"5.6.20\";s:13:\"compatibility\";O:8:\"stdClass\":0:{}}s:29:\"health-check/health-check.php\";O:8:\"stdClass\":13:{s:2:\"id\";s:26:\"w.org/plugins/health-check\";s:4:\"slug\";s:12:\"health-check\";s:6:\"plugin\";s:29:\"health-check/health-check.php\";s:11:\"new_version\";s:5:\"1.7.0\";s:3:\"url\";s:43:\"https://wordpress.org/plugins/health-check/\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/plugin/health-check.1.7.0.zip\";s:5:\"icons\";a:2:{s:2:\"1x\";s:57:\"https://ps.w.org/health-check/assets/icon.svg?rev=1828244\";s:3:\"svg\";s:57:\"https://ps.w.org/health-check/assets/icon.svg?rev=1828244\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:68:\"https://ps.w.org/health-check/assets/banner-1544x500.png?rev=1823210\";s:2:\"1x\";s:67:\"https://ps.w.org/health-check/assets/banner-772x250.png?rev=1823210\";}s:11:\"banners_rtl\";a:0:{}s:8:\"requires\";s:3:\"4.4\";s:6:\"tested\";s:5:\"6.4.2\";s:12:\"requires_php\";s:3:\"5.6\";s:13:\"compatibility\";O:8:\"stdClass\":0:{}}s:47:\"jwt-authentication-for-wp-rest-api/jwt-auth.php\";O:8:\"stdClass\":13:{s:2:\"id\";s:48:\"w.org/plugins/jwt-authentication-for-wp-rest-api\";s:4:\"slug\";s:34:\"jwt-authentication-for-wp-rest-api\";s:6:\"plugin\";s:47:\"jwt-authentication-for-wp-rest-api/jwt-auth.php\";s:11:\"new_version\";s:5:\"1.3.4\";s:3:\"url\";s:65:\"https://wordpress.org/plugins/jwt-authentication-for-wp-rest-api/\";s:7:\"package\";s:83:\"https://downloads.wordpress.org/plugin/jwt-authentication-for-wp-rest-api.1.3.4.zip\";s:5:\"icons\";a:2:{s:2:\"1x\";s:79:\"https://ps.w.org/jwt-authentication-for-wp-rest-api/assets/icon.svg?rev=2787935\";s:3:\"svg\";s:79:\"https://ps.w.org/jwt-authentication-for-wp-rest-api/assets/icon.svg?rev=2787935\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:89:\"https://ps.w.org/jwt-authentication-for-wp-rest-api/assets/banner-772x250.jpg?rev=2787935\";}s:11:\"banners_rtl\";a:0:{}s:8:\"requires\";s:3:\"4.2\";s:6:\"tested\";s:5:\"6.3.2\";s:12:\"requires_php\";s:5:\"7.4.0\";s:13:\"compatibility\";O:8:\"stdClass\":0:{}}s:48:\"capability-manager-enhanced/capsman-enhanced.php\";O:8:\"stdClass\":13:{s:2:\"id\";s:41:\"w.org/plugins/capability-manager-enhanced\";s:4:\"slug\";s:27:\"capability-manager-enhanced\";s:6:\"plugin\";s:48:\"capability-manager-enhanced/capsman-enhanced.php\";s:11:\"new_version\";s:6:\"2.11.1\";s:3:\"url\";s:58:\"https://wordpress.org/plugins/capability-manager-enhanced/\";s:7:\"package\";s:77:\"https://downloads.wordpress.org/plugin/capability-manager-enhanced.2.11.1.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:80:\"https://ps.w.org/capability-manager-enhanced/assets/icon-256x256.png?rev=2977153\";s:2:\"1x\";s:80:\"https://ps.w.org/capability-manager-enhanced/assets/icon-128x128.png?rev=2977153\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:83:\"https://ps.w.org/capability-manager-enhanced/assets/banner-1544x500.png?rev=3010239\";s:2:\"1x\";s:82:\"https://ps.w.org/capability-manager-enhanced/assets/banner-772x250.png?rev=3010239\";}s:11:\"banners_rtl\";a:0:{}s:8:\"requires\";s:3:\"5.5\";s:6:\"tested\";s:5:\"6.4.2\";s:12:\"requires_php\";s:5:\"7.2.5\";s:13:\"compatibility\";O:8:\"stdClass\":0:{}}s:35:\"hide-login-page/hide-login-page.php\";O:8:\"stdClass\":13:{s:2:\"id\";s:29:\"w.org/plugins/hide-login-page\";s:4:\"slug\";s:15:\"hide-login-page\";s:6:\"plugin\";s:35:\"hide-login-page/hide-login-page.php\";s:11:\"new_version\";s:5:\"1.1.9\";s:3:\"url\";s:46:\"https://wordpress.org/plugins/hide-login-page/\";s:7:\"package\";s:58:\"https://downloads.wordpress.org/plugin/hide-login-page.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:68:\"https://ps.w.org/hide-login-page/assets/icon-256x256.png?rev=1849330\";s:2:\"1x\";s:68:\"https://ps.w.org/hide-login-page/assets/icon-128x128.png?rev=1849330\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:71:\"https://ps.w.org/hide-login-page/assets/banner-1544x500.jpg?rev=1849330\";s:2:\"1x\";s:70:\"https://ps.w.org/hide-login-page/assets/banner-772x250.jpg?rev=1849330\";}s:11:\"banners_rtl\";a:0:{}s:8:\"requires\";s:3:\"5.2\";s:6:\"tested\";s:5:\"6.4.2\";s:12:\"requires_php\";s:3:\"7.0\";s:13:\"compatibility\";O:8:\"stdClass\":0:{}}}s:12:\"translations\";a:0:{}s:9:\"no_update\";a:5:{s:41:\"acf-to-rest-api/class-acf-to-rest-api.php\";O:8:\"stdClass\":10:{s:2:\"id\";s:29:\"w.org/plugins/acf-to-rest-api\";s:4:\"slug\";s:15:\"acf-to-rest-api\";s:6:\"plugin\";s:41:\"acf-to-rest-api/class-acf-to-rest-api.php\";s:11:\"new_version\";s:5:\"3.3.3\";s:3:\"url\";s:46:\"https://wordpress.org/plugins/acf-to-rest-api/\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/plugin/acf-to-rest-api.3.3.3.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:68:\"https://ps.w.org/acf-to-rest-api/assets/icon-256x256.jpg?rev=1752896\";s:2:\"1x\";s:68:\"https://ps.w.org/acf-to-rest-api/assets/icon-128x128.jpg?rev=1752896\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:71:\"https://ps.w.org/acf-to-rest-api/assets/banner-1544x500.jpg?rev=1752896\";s:2:\"1x\";s:70:\"https://ps.w.org/acf-to-rest-api/assets/banner-772x250.jpg?rev=1752896\";}s:11:\"banners_rtl\";a:0:{}s:8:\"requires\";s:3:\"4.6\";}s:31:\"headless-mode/headless-mode.php\";O:8:\"stdClass\":10:{s:2:\"id\";s:27:\"w.org/plugins/headless-mode\";s:4:\"slug\";s:13:\"headless-mode\";s:6:\"plugin\";s:31:\"headless-mode/headless-mode.php\";s:11:\"new_version\";s:5:\"0.4.0\";s:3:\"url\";s:44:\"https://wordpress.org/plugins/headless-mode/\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/plugin/headless-mode.0.4.0.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:66:\"https://ps.w.org/headless-mode/assets/icon-256x256.jpg?rev=2212212\";s:2:\"1x\";s:66:\"https://ps.w.org/headless-mode/assets/icon-128x128.jpg?rev=2212212\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:68:\"https://ps.w.org/headless-mode/assets/banner-772x250.jpg?rev=2212212\";}s:11:\"banners_rtl\";a:1:{s:2:\"1x\";s:72:\"https://ps.w.org/headless-mode/assets/banner-772x250-rtl.jpg?rev=2212212\";}s:8:\"requires\";s:3:\"5.0\";}s:43:\"bdvs-password-reset/bdvs-password-reset.php\";O:8:\"stdClass\":10:{s:2:\"id\";s:33:\"w.org/plugins/bdvs-password-reset\";s:4:\"slug\";s:19:\"bdvs-password-reset\";s:6:\"plugin\";s:43:\"bdvs-password-reset/bdvs-password-reset.php\";s:11:\"new_version\";s:6:\"0.0.16\";s:3:\"url\";s:50:\"https://wordpress.org/plugins/bdvs-password-reset/\";s:7:\"package\";s:69:\"https://downloads.wordpress.org/plugin/bdvs-password-reset.0.0.16.zip\";s:5:\"icons\";a:1:{s:2:\"1x\";s:72:\"https://ps.w.org/bdvs-password-reset/assets/icon-128x128.jpg?rev=2301474\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:75:\"https://ps.w.org/bdvs-password-reset/assets/banner-1544x500.jpg?rev=2301474\";s:2:\"1x\";s:74:\"https://ps.w.org/bdvs-password-reset/assets/banner-772x250.jpg?rev=2301474\";}s:11:\"banners_rtl\";a:2:{s:2:\"2x\";s:79:\"https://ps.w.org/bdvs-password-reset/assets/banner-1544x500-rtl.jpg?rev=2301474\";s:2:\"1x\";s:78:\"https://ps.w.org/bdvs-password-reset/assets/banner-772x250-rtl.jpg?rev=2301474\";}s:8:\"requires\";s:3:\"4.6\";}s:24:\"wp-admin-cache/index.php\";O:8:\"stdClass\":10:{s:2:\"id\";s:28:\"w.org/plugins/wp-admin-cache\";s:4:\"slug\";s:14:\"wp-admin-cache\";s:6:\"plugin\";s:24:\"wp-admin-cache/index.php\";s:11:\"new_version\";s:5:\"0.2.7\";s:3:\"url\";s:45:\"https://wordpress.org/plugins/wp-admin-cache/\";s:7:\"package\";s:57:\"https://downloads.wordpress.org/plugin/wp-admin-cache.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:67:\"https://ps.w.org/wp-admin-cache/assets/icon-256x256.png?rev=2047089\";s:2:\"1x\";s:67:\"https://ps.w.org/wp-admin-cache/assets/icon-128x128.png?rev=2047089\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:70:\"https://ps.w.org/wp-admin-cache/assets/banner-1544x500.jpg?rev=2047089\";s:2:\"1x\";s:69:\"https://ps.w.org/wp-admin-cache/assets/banner-772x250.jpg?rev=2047089\";}s:11:\"banners_rtl\";a:0:{}s:8:\"requires\";s:3:\"4.6\";}s:23:\"wp-avatar/wp-avatar.php\";O:8:\"stdClass\":10:{s:2:\"id\";s:23:\"w.org/plugins/wp-avatar\";s:4:\"slug\";s:9:\"wp-avatar\";s:6:\"plugin\";s:23:\"wp-avatar/wp-avatar.php\";s:11:\"new_version\";s:5:\"1.0.0\";s:3:\"url\";s:40:\"https://wordpress.org/plugins/wp-avatar/\";s:7:\"package\";s:52:\"https://downloads.wordpress.org/plugin/wp-avatar.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:62:\"https://ps.w.org/wp-avatar/assets/icon-256x256.png?rev=1787902\";s:2:\"1x\";s:62:\"https://ps.w.org/wp-avatar/assets/icon-128x128.png?rev=1787902\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:65:\"https://ps.w.org/wp-avatar/assets/banner-1544x500.jpg?rev=1780844\";s:2:\"1x\";s:64:\"https://ps.w.org/wp-avatar/assets/banner-772x250.jpg?rev=1780847\";}s:11:\"banners_rtl\";a:0:{}s:8:\"requires\";s:3:\"4.8\";}}}', 'no'),
(80733, '_site_transient_timeout_theme_roots', '1705322031', 'no'),
(80734, '_site_transient_theme_roots', 'a:3:{s:6:\"assets\";s:7:\"/themes\";s:7:\"mangird\";s:7:\"/themes\";s:7:\"nofront\";s:7:\"/themes\";}', 'no'),
(80745, '_transient_doing_cron', '1705487410.4988169670104980468750', 'yes');

-- --------------------------------------------------------

--
-- Table structure for table `wp_order_shippings`
--

DROP TABLE IF EXISTS `wp_order_shippings`;
CREATE TABLE IF NOT EXISTS `wp_order_shippings` (
  `id` int NOT NULL AUTO_INCREMENT,
  `order_id` int DEFAULT NULL,
  `shipping_id` int DEFAULT NULL,
  `status` mediumtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `author` int DEFAULT NULL,
  `shipment_contents` mediumtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `reason` mediumtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `datestamp` varchar(300) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_postmeta`
--

DROP TABLE IF EXISTS `wp_postmeta`;
CREATE TABLE IF NOT EXISTS `wp_postmeta` (
  `meta_id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `post_id` bigint UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`meta_id`),
  KEY `post_id` (`post_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_posts`
--

DROP TABLE IF EXISTS `wp_posts`;
CREATE TABLE IF NOT EXISTS `wp_posts` (
  `ID` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `post_author` bigint UNSIGNED NOT NULL DEFAULT '0',
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_title` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_excerpt` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_status` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'open',
  `post_password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `post_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `to_ping` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `pinged` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_parent` bigint UNSIGNED NOT NULL DEFAULT '0',
  `guid` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `menu_order` int NOT NULL DEFAULT '0',
  `post_type` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_count` bigint NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `post_name` (`post_name`(191)),
  KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  KEY `post_parent` (`post_parent`),
  KEY `post_author` (`post_author`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_termmeta`
--

DROP TABLE IF EXISTS `wp_termmeta`;
CREATE TABLE IF NOT EXISTS `wp_termmeta` (
  `meta_id` bigint UNSIGNED NOT NULL,
  `term_id` bigint UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`meta_id`),
  KEY `term_id` (`term_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_terms`
--

DROP TABLE IF EXISTS `wp_terms`;
CREATE TABLE IF NOT EXISTS `wp_terms` (
  `term_id` bigint UNSIGNED NOT NULL,
  `name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `slug` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `term_group` bigint NOT NULL DEFAULT '0',
  PRIMARY KEY (`term_id`),
  KEY `slug` (`slug`(191)),
  KEY `name` (`name`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_term_relationships`
--

DROP TABLE IF EXISTS `wp_term_relationships`;
CREATE TABLE IF NOT EXISTS `wp_term_relationships` (
  `object_id` bigint UNSIGNED NOT NULL DEFAULT '0',
  `term_taxonomy_id` bigint UNSIGNED NOT NULL DEFAULT '0',
  `term_order` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`object_id`),
  KEY `term_taxonomy_id` (`term_taxonomy_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_term_taxonomy`
--

DROP TABLE IF EXISTS `wp_term_taxonomy`;
CREATE TABLE IF NOT EXISTS `wp_term_taxonomy` (
  `term_taxonomy_id` bigint UNSIGNED NOT NULL,
  `term_id` bigint UNSIGNED NOT NULL DEFAULT '0',
  `taxonomy` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `description` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent` bigint UNSIGNED NOT NULL DEFAULT '0',
  `count` bigint NOT NULL DEFAULT '0',
  PRIMARY KEY (`term_taxonomy_id`),
  UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  KEY `taxonomy` (`taxonomy`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_usermeta`
--

DROP TABLE IF EXISTS `wp_usermeta`;
CREATE TABLE IF NOT EXISTS `wp_usermeta` (
  `umeta_id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`umeta_id`),
  KEY `user_id` (`user_id`),
  KEY `meta_key` (`meta_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_users`
--

DROP TABLE IF EXISTS `wp_users`;
CREATE TABLE IF NOT EXISTS `wp_users` (
  `ID` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_login` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_pass` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_nicename` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_email` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_url` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_status` int NOT NULL DEFAULT '0',
  `display_name` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`ID`),
  KEY `user_login` (`user_login`),
  KEY `user_nicename` (`user_nicename`),
  KEY `user_login_key` (`user_login`),
  KEY `user_email` (`user_email`)
) ENGINE=InnoDB AUTO_INCREMENT=87 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_users`
--

INSERT INTO `wp_users` (`ID`, `user_login`, `user_pass`, `user_nicename`, `user_email`, `user_url`, `user_registered`, `user_activation_key`, `user_status`, `display_name`) VALUES
(1, 'manGrid', '$2y$10$p6zQWgfKS/aRmss8V3sow.YUMprQ6Eu3amYXEGxTlfU2KwHZ80YTy', 'mangrid', 'lukas@talacka.com', '', '2018-11-12 12:03:50', '', 0, 'Lukas Talacka');

-- --------------------------------------------------------

--
-- Table structure for table `wp_users_lukas`
--

DROP TABLE IF EXISTS `wp_users_lukas`;
CREATE TABLE IF NOT EXISTS `wp_users_lukas` (
  `ID` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_login` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_pass` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_nicename` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_email` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_url` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_status` int NOT NULL DEFAULT '0',
  `display_name` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`ID`),
  KEY `user_login` (`user_login`),
  KEY `user_nicename` (`user_nicename`),
  KEY `user_login_key` (`user_login`),
  KEY `user_email` (`user_email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `chat_message`
--
ALTER TABLE `chat_message`
  ADD CONSTRAINT `chat_message_ibfk_1` FOREIGN KEY (`room_id`) REFERENCES `chat_room` (`id`),
  ADD CONSTRAINT `chat_message_ibfk_2` FOREIGN KEY (`participant_id`) REFERENCES `chat_participants` (`id`);

--
-- Constraints for table `chat_message_files`
--
ALTER TABLE `chat_message_files`
  ADD CONSTRAINT `chat_message_files_ibfk_1` FOREIGN KEY (`message_id`) REFERENCES `chat_message` (`id`);

--
-- Constraints for table `chat_message_status`
--
ALTER TABLE `chat_message_status`
  ADD CONSTRAINT `chat_message_status_ibfk_1` FOREIGN KEY (`message_id`) REFERENCES `chat_message` (`id`),
  ADD CONSTRAINT `chat_message_status_ibfk_2` FOREIGN KEY (`participant_id`) REFERENCES `chat_participants` (`id`);

--
-- Constraints for table `chat_participants`
--
ALTER TABLE `chat_participants`
  ADD CONSTRAINT `chat_participants_ibfk_1` FOREIGN KEY (`room_id`) REFERENCES `chat_room` (`id`);

--
-- Constraints for table `country_post`
--
ALTER TABLE `country_post`
  ADD CONSTRAINT `country_post_ibfk_1` FOREIGN KEY (`country_id`) REFERENCES `countries_settings` (`id`);

--
-- Constraints for table `country_user`
--
ALTER TABLE `country_user`
  ADD CONSTRAINT `country_user_ibfk_1` FOREIGN KEY (`country_id`) REFERENCES `countries_settings` (`id`);

--
-- Constraints for table `factory_post`
--
ALTER TABLE `factory_post`
  ADD CONSTRAINT `factory_post_ibfk_1` FOREIGN KEY (`factory_id`) REFERENCES `factories_settings` (`id`);

--
-- Constraints for table `factory_user`
--
ALTER TABLE `factory_user`
  ADD CONSTRAINT `factory_user_ibfk_1` FOREIGN KEY (`factory_id`) REFERENCES `factories_settings` (`id`);

--
-- Constraints for table `mounting_crew_user`
--
ALTER TABLE `mounting_crew_user`
  ADD CONSTRAINT `mounting_crew_user_ibfk_1` FOREIGN KEY (`crew_id`) REFERENCES `mounting_crews` (`id`);

--
-- Constraints for table `notifications_data`
--
ALTER TABLE `notifications_data`
  ADD CONSTRAINT `notifications_data_ibfk_1` FOREIGN KEY (`notif_id`) REFERENCES `notifications` (`id`);

--
-- Constraints for table `notifications_users_data`
--
ALTER TABLE `notifications_users_data`
  ADD CONSTRAINT `notifications_users_data_ibfk_1` FOREIGN KEY (`notif_id`) REFERENCES `notifications_data` (`id`);

--
-- Constraints for table `origin_user`
--
ALTER TABLE `origin_user`
  ADD CONSTRAINT `origin_user_ibfk_1` FOREIGN KEY (`country_id`) REFERENCES `countries_settings` (`id`);

--
-- Constraints for table `product_input_table`
--
ALTER TABLE `product_input_table`
  ADD CONSTRAINT `product_input_table_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `product_table` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `product_input_values`
--
ALTER TABLE `product_input_values`
  ADD CONSTRAINT `product_input_values_ibfk_1` FOREIGN KEY (`input_id`) REFERENCES `product_input_table` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `wp_mounting_jobs_assigned`
--
ALTER TABLE `wp_mounting_jobs_assigned`
  ADD CONSTRAINT `wp_mounting_jobs_assigned_ibfk_1` FOREIGN KEY (`job_id`) REFERENCES `wp_mounting_jobs` (`id`);

--
-- Constraints for table `wp_notifications_multi`
--
ALTER TABLE `wp_notifications_multi`
  ADD CONSTRAINT `wp_notifications_multi_ibfk_1` FOREIGN KEY (`notif_id`) REFERENCES `wp_notifications` (`id`);

--
-- Constraints for table `wp_notifications_multi_users`
--
ALTER TABLE `wp_notifications_multi_users`
  ADD CONSTRAINT `wp_notifications_multi_users_ibfk_1` FOREIGN KEY (`change_id`) REFERENCES `wp_notifications_multi` (`id`);

--
-- Constraints for table `wp_notifications_users`
--
ALTER TABLE `wp_notifications_users`
  ADD CONSTRAINT `wp_notifications_users_ibfk_1` FOREIGN KEY (`notif_id`) REFERENCES `wp_notifications` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
