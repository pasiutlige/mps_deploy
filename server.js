const cluster = require('cluster');
const numCPUs = require('os').cpus().length;
const express = require('express');
const { spawn } = require('child_process');
const { exec } = require('child_process');
const path = require('path');
const fs = require('fs');
const yaml = require('js-yaml');
const app = express();
const port = 3000;

require('dotenv').config();
const mysqlRootUser = process.env.MYSQL_ROOT_USER;
const mysqlRootPassword = process.env.MYSQL_ROOT_PASSWORD;

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

if (cluster.isMaster) {
	// Create worker processes based on the number of CPU cores
	for (let i = 0; i < numCPUs; i++) {
		cluster.fork();
	}

	cluster.on('exit', (worker, code, signal) => {
		console.log(`Worker ${worker.process.pid} died`);
	});
} else {
	console.log('Node.js version:', process.version);
	// Inside worker process
	app.post('/api/deploy', (req, res) => {
		// we are saving ALL the stuff that gets vomited in to here to gather variables after
		let fullOutput = '';
		console.log({ body: req.body });

		const subdomain = req.body.subdomain;
		const email = req.body.email;
		const name = req.body.name;
		const users = req.body.users;
		const passwordHash = req.body.passwordHash;
		const scriptPath = './deploy.sh';

		if (!subdomain) {
			console.log('Subdomain not provided!');
			return res.status(400).json({ error: 'Subdomain not provided' });
		}

		console.log('Deployment endpoint triggered!');

		// Generate a secure random password
		const generatedPassword = generateRandomPassword();

		console.log({ subdomain });
		console.log({ email });
		console.log({ name });
		console.log({ users });
		console.log({ passwordHash });

		// Execute the deployment script with proper error handling
		const deployProcess = spawn('nice', [
			'-n',
			'19',
			'ionice',
			'-c3',
			'sh',
			scriptPath,
			subdomain,
			generatedPassword,
			email,
			name,
			users,
			passwordHash,
		]);

		deployProcess.stdout.on('data', data => {
			console.log(`Deployment output: ${data}`);
			fullOutput += data; // Append the data to the full output
		});

		deployProcess.stderr.on('data', data => {
			console.error(`${data}`);
		});

		deployProcess.on('close', code => {
			if (code === 0) {
				console.log(fullOutput);

				const lines = fullOutput.split('\n'); // Split by new line
				let wpContainerName, dbContainerName;

				lines.forEach(line => {
					if (line.includes('WP Container Name:')) {
						wpContainerName = line.split('WP Container Name:')[1]?.trim();
					}
					if (line.includes('DB Container Name:')) {
						dbContainerName = line.split('DB Container Name:')[1]?.trim();
					}
				});

				console.log('Deployment successful');
				console.log(generatedPassword);
				console.log('Parsed WP Container ID:', wpContainerName);
				console.log('Parsed DB Container ID:', dbContainerName);
				res.status(200).json({
					message: 'Deployment successful',
					wpContainerName: wpContainerName,
					dbContainerName: dbContainerName,
					systemUrl: `https://${subdomain}.scaleerp.com`,
				});
			} else {
				console.error(`Error during deployment. Exit code: ${code}`);
				res.status(500).json({ error: 'Deployment failed' });
			}
		});
	});

	app.listen(port, () => {
		console.log(`Worker ${process.pid} is running on http://128.140.110.88:${port}`);
	});

	app.post('/api/update', (req, res) => {
		const { containerName, users, status } = req.body; // Accept container name, users, and status

		if (!containerName || !users || status === undefined) {
			// Check for the container name, users, and status
			console.error('Required parameters not provided!');
			return res.status(400).json({ error: 'Required parameters not provided' });
		}

		console.log('Update endpoint triggered!');
		console.log(`Container Name: ${containerName}`);
		console.log(`New User Limit: ${users}`);
		console.log(`Subscription Status: ${status}`);
		console.log(`Root User: ${mysqlRootUser}`);
		console.log(`Root Password: ${mysqlRootPassword}`);

		// Extract the subdomain from the container name (if needed)
		const subdomainMatch = containerName.match(/mps_deploy_([a-zA-Z0-9-]+)_db-1/);
		if (!subdomainMatch) {
			console.error('Invalid container name format.');
			return res.status(400).json({ error: 'Invalid container name format.' });
		}
		const fullSubdomain = subdomainMatch[1];
		// Assuming the database name uses the first part of the subdomain before the first '-'
		const databasePart = fullSubdomain.split('-')[0];
		const mysqlDatabase = `${databasePart}_db`; // Construct the database name

		// SQL commands to update user limit, subscription status, and subscription date
		const userLimitUpdate = `UPDATE wp_options SET option_value = '${users}' WHERE option_name = 'user_limit';`;
		const subscriptionStatusUpdate = `UPDATE wp_options SET option_value = '${status}' WHERE option_name = 'subscription_status';`;
		const subscriptionDateUpdate = `UPDATE wp_options SET option_value = NOW() WHERE option_name = 'subscription_date';`;

		const sqlCommands = `${userLimitUpdate} ${subscriptionStatusUpdate} ${subscriptionDateUpdate}`;

		exec(
			`docker exec ${containerName} mysql -u${mysqlRootUser} -p${mysqlRootPassword} ${mysqlDatabase} -sse "${sqlCommands}"`,
			(error, stdout, stderr) => {
				if (error) {
					console.error(`Exec error: ${error}`);
					return res.status(500).json({ error: 'Database update failed' });
				}
				if (stderr) {
					console.error(`Exec stderr: ${stderr}`);
				}

				console.log(`Exec stdout: ${stdout}`);
				res.status(200).json({ message: 'Update successful' });
			}
		);
	});

	app.post('/api/shutdown', (req, res) => {
		const { wpContainerName, dbContainerName } = req.body;

		if (!wpContainerName || !dbContainerName) {
			console.log('Required parameters not provided!');
			return res.status(400).json({ error: 'Required parameters not provided' });
		}

		console.log('Shutdown endpoint triggered!');
		console.log(`WordPress Container Name: ${wpContainerName}`);
		console.log(`MySQL Container Name: ${dbContainerName}`);

		// Stop the WordPress container by name
		exec(`docker stop ${wpContainerName}`, (wpError, wpStdout, wpStderr) => {
			if (wpError) {
				console.error(`Error during WordPress container shutdown: ${wpError.message}`);
				return res.status(500).json({ error: 'WordPress container shutdown failed' });
			}
			if (wpStderr) {
				console.error(`WordPress container shutdown stderr: ${wpStderr}`);
				return res.status(500).json({ error: 'WordPress container shutdown failed' });
			}
			console.log(`WordPress container shutdown stdout: ${wpStdout}`);

			// Stop the MySQL container by name
			exec(`docker stop ${dbContainerName}`, (dbError, dbStdout, dbStderr) => {
				if (dbError) {
					console.error(`Error during MySQL container shutdown: ${dbError.message}`);
					return res.status(500).json({ error: 'MySQL container shutdown failed' });
				}
				if (dbStderr) {
					console.error(`MySQL container shutdown stderr: ${dbStderr}`);
					return res.status(500).json({ error: 'MySQL container shutdown failed' });
				}
				console.log(`MySQL container shutdown stdout: ${dbStdout}`);

				// If both containers are stopped successfully
				res.status(200).json({ message: 'Shutdown successful for both containers' });
			});
		});
	});

	app.post('/api/start', (req, res) => {
		const { wpContainerName, dbContainerName } = req.body;

		if (!wpContainerName || !dbContainerName) {
			console.log('Required parameters not provided!');
			return res.status(400).json({ error: 'Required parameters not provided' });
		}

		console.log('Start endpoint triggered!');
		console.log(`WordPress Container Name: ${wpContainerName}`);
		console.log(`MySQL Container Name: ${dbContainerName}`);

		// Start the MySQL container by name first (dependency order)
		exec(`docker start ${dbContainerName}`, (dbError, dbStdout, dbStderr) => {
			if (dbError) {
				console.error(`Error during MySQL container start: ${dbError.message}`);
				return res.status(500).json({ error: 'MySQL container start failed' });
			}
			if (dbStderr) {
				console.error(`MySQL container start stderr: ${dbStderr}`);
				return res.status(500).json({ error: 'MySQL container start failed' });
			}
			console.log(`MySQL container start stdout: ${dbStdout}`);

			// Start the WordPress container by name
			exec(`docker start ${wpContainerName}`, (wpError, wpStdout, wpStderr) => {
				if (wpError) {
					console.error(`Error during WordPress container start: ${wpError.message}`);
					return res.status(500).json({ error: 'WordPress container start failed' });
				}
				if (wpStderr) {
					console.error(`WordPress container start stderr: ${wpStderr}`);
					return res.status(500).json({ error: 'WordPress container start failed' });
				}
				console.log(`WordPress container start stdout: ${wpStdout}`);

				// If both containers are started successfully
				res.status(200).json({ message: 'Start successful for both containers' });
			});
		});
	});

	app.post('/api/add-subdomain', (req, res) => {
		const { containerName, newSubdomain } = req.body;

		if (!containerName || !newSubdomain) {
			console.error('Required parameters not provided!');
			return res.status(400).json({ error: 'Required parameters not provided' });
		}

		console.log('Add Subdomain endpoint triggered!');
		console.log(`Container Name: ${containerName}`);
		console.log(`New Subdomain: ${newSubdomain}`);

		// Extract the subdomain part from the container name
		const subdomainMatch = containerName.match(/mps_deploy_([a-zA-Z0-9-]+)_wordpress-1/);
		if (!subdomainMatch) {
			console.error('Invalid container name format.');
			return res.status(400).json({ error: 'Invalid container name format.' });
		}
		const fullSubdomain = subdomainMatch[1];
		// Assuming the database name uses the first part of the subdomain before the first '-'
		const subdomain = fullSubdomain.split('-')[0];

		console.log('THIS IS THE SUBDOMAIN WE TOOK BABY!');
		console.log(subdomain);

		// Determine the path to the docker-compose file
		const dockerComposePath = `./docker-compose-${subdomain}.yml`;
		console.log(`Docker Compose Path: ${dockerComposePath}`);

		// Proceed to read, modify, and write the docker-compose file
		modifyDockerCompose(dockerComposePath, subdomain, newSubdomain, res);
	});

	app.post('/api/check-subdomain', (req, res) => {
		const { subdomain } = req.body;

		if (!subdomain) {
			console.error('Required parameters not provided!');
			return res.status(400).json({ error: 'Required parameters not provided' });
		}

		exec(
			`docker ps --filter "name=${subdomain}" --format "{{.Names}}"`,
			(error, stdout, stderr) => {
				if (error) {
					console.error(`Exec error: ${error}`);
					return res.status(500).json({ error: 'Database update failed' });
				}
				if (stderr) {
					console.error(`Exec stderr: ${stderr}`);
				}
				console.log(stdout);

				console.log(`Exec stdout: ${stdout}`);
				res.status(200).json({ message: 'Update successful' });
			}
		);
	});
}

// Function to generate a secure random password
function generateRandomPassword() {
	const length = 12; // You can adjust the length as needed
	const charset = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()_-+=';
	let password = '';

	for (let i = 0; i < length; i++) {
		const randomIndex = Math.floor(Math.random() * charset.length);
		password += charset[randomIndex];
	}

	return password;
}

function modifyDockerCompose(filePath, subdomain, newSubdomain, res) {
	fs.readFile(filePath, 'utf8', (err, fileContent) => {
		if (err) {
			console.error(`Error reading ${filePath}: ${err}`);
			return res.status(500).json({ error: `Failed to read ${filePath}` });
		}

		// Parse the YAML file content
		let doc;
		try {
			doc = yaml.load(fileContent);
		} catch (e) {
			console.error(`Error parsing ${filePath}: ${e}`);
			return res.status(500).json({ error: `Failed to parse ${filePath}` });
		}

		// Modify the document: Add new labels for Traefik for the new subdomain
		const labels = doc.services[`${subdomain}_wordpress`].labels;

		// Construct the labels for the new subdomain
		const newLabels = [
			`traefik.http.routers.wordpress-${newSubdomain}.rule=Host(\`${newSubdomain}.scaleerp.com\`)`,
			`traefik.http.routers.wordpress-${newSubdomain}.tls=true`, // Enable TLS
			`traefik.http.routers.wordpress-${newSubdomain}.tls.certresolver=myresolver`, // Use the certificate resolver
			`traefik.http.routers.wordpress-${newSubdomain}.service=wordpress-service-${subdomain}`,
		];

		// Add the new labels for the new subdomain
		newLabels.forEach(label => labels.push(label));

		// Ensure the existing router also points to the common service
		const existingServiceIndex = labels.findIndex(label =>
			label.startsWith(`traefik.http.routers.wordpress-${subdomain}.service=`)
		);
		if (existingServiceIndex !== -1) {
			// Update existing service label to point to the common service
			labels[
				existingServiceIndex
			] = `traefik.http.routers.wordpress-${subdomain}.service=wordpress-service-${subdomain}`;
		} else {
			// If the service label doesn't exist, add it
			labels.push(
				`traefik.http.routers.wordpress-${subdomain}.service=wordpress-service-${subdomain}`
			);
		}

		// Check if the common service label is already set, if not, add it
		const commonServiceLabel = `traefik.http.services.wordpress-service-${subdomain}.loadbalancer.server.port=80`;
		if (!labels.includes(commonServiceLabel)) {
			labels.push(commonServiceLabel);
		}

		// Convert the JS object back to a YAML string
		let newYamlContent;
		try {
			newYamlContent = yaml.dump(doc);
		} catch (e) {
			console.error(`Error stringifying the YAML content: ${e}`);
			return res.status(500).json({ error: 'Failed to stringify the YAML content' });
		}

		// Write the new YAML content back to the file
		fs.writeFile(filePath, newYamlContent, 'utf8', err => {
			if (err) {
				console.error(`Error writing to ${filePath}: ${err}`);
				return res.status(500).json({ error: `Failed to write to ${filePath}` });
			}

			// Re-deploy the services
			console.log(`Successfully updated ${filePath}`);
			redeployServices(subdomain, res); // Ensure this function correctly redeploys the services
		});
	});
}

function redeployServices(subdomain, res) {
	exec(
		`docker-compose -p mps_deploy_${subdomain} -f docker-compose-${subdomain}.yml up -d --force-recreate`,
		(error, stdout, stderr) => {
			console.log(`Redeployment stdout: ${stdout}`);
			res.status(200).json({ message: 'Subdomain added and services redeployed successfully' });
		}
	);
}

function extractSubdomainFromFileName(fileName) {
	// Split the filename by '-' and get the second part (e.g., "system3.yml")
	const parts = fileName.split('-')[2];

	// Remove the ".yml" extension to get the subdomain
	const subdomain = parts.replace('.yml', '');

	return subdomain;
}
