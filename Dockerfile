# Use an official PHP image as the base image
FROM php:7.4-apache

ARG SUBDOMAIN
ENV VIRTUAL_HOST=$SUBDOMAIN.oscerp.com
ENV WORDPRESS_DB_NAME=$SUBDOMAIN
ENV WORDPRESS_DB_USER=$SUBDOMAIN
ENV WORDPRESS_DB_PASSWORD=$SUBDOMAIN
ENV WORDPRESS_DB_HOST=db:3306

# Enable Apache module rewrite
RUN a2enmod rewrite

# Install PHP extensions
RUN docker-php-ext-install mysqli pdo pdo_mysql

# Install wp-cli
RUN curl -O https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar \
    && chmod +x wp-cli.phar \
    && mv wp-cli.phar /usr/local/bin/wp

# Install system dependencies including cron
RUN apt-get update && apt-get install -y cron \
    && apt-get clean && rm -rf /var/lib/apt/lists/*

# Set environment variables for Apache to pass to PHP
RUN echo "SetEnv WORDPRESS_DB_HOST ${WORDPRESS_DB_HOST}" >> /etc/apache2/apache2.conf \
    && echo "SetEnv WORDPRESS_DB_USER ${WORDPRESS_DB_USER}" >> /etc/apache2/apache2.conf \
    && echo "SetEnv WORDPRESS_DB_PASSWORD ${WORDPRESS_DB_PASSWORD}" >> /etc/apache2/apache2.conf \
    && echo "SetEnv WORDPRESS_DB_NAME ${WORDPRESS_DB_NAME}" >> /etc/apache2/apache2.conf \
    && echo "SetEnv VIRTUAL_HOST ${VIRTUAL_HOST}" >> /etc/apache2/apache2.conf

# Pass environment variables to Apache
RUN { \
    echo 'PassEnv WORDPRESS_DB_HOST'; \
    echo 'PassEnv WORDPRESS_DB_USER'; \
    echo 'PassEnv WORDPRESS_DB_PASSWORD'; \
    echo 'PassEnv WORDPRESS_DB_NAME'; \
    echo 'PassEnv VIRTUAL_HOST'; \
    } >> /etc/apache2/conf-available/envvars.conf \
    && a2enconf envvars

# ---------------------- Include Your Cron Job Setup Here ----------------------
# Copy cron job file to the container (make sure the wordpress-cron file exists in your context)
COPY wordpress-cron /etc/cron.d/wordpress-cron

# Give execution rights on the cron job
RUN chmod 0644 /etc/cron.d/wordpress-cron

# Apply the cron job
RUN crontab /etc/cron.d/wordpress-cron
# ------------------------------------------------------------------------------

# Prepare the start script to run cron and Apache
RUN echo '#!/bin/bash\n\n# Start cron\nservice cron start\n\n# Start Apache in the foreground\napache2-foreground' > /usr/local/bin/start.sh \
    && chmod +x /usr/local/bin/start.sh

# Use the start script to start cron and Apache
CMD ["/usr/local/bin/start.sh"]