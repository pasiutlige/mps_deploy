#!/bin/bash
# Usage: ./deploy.sh subdomain

# Check if all required parameters are provided
if [ $# -ne 6 ]; then
  echo "Usage: ./deploy.sh subdomain generated_password email name users password_hash"
  exit 1
fi

SUBDOMAIN=$1
GENERATED_PASSWORD=$2
EMAIL=$3
NAME=$4
USERS=$5
PASSWORDHASH=$6

# Set environment variables
export SUBDOMAIN
export GENERATED_PASSWORD
export EMAIL
export NAME
export MYSQL_DATABASE="${SUBDOMAIN}_db"
export MYSQL_USER="${SUBDOMAIN}_user"
export MYSQL_PASSWORD="${SUBDOMAIN}_password"
export MYSQL_ROOT_PASSWORD="your_root_password" # Consider a more secure approach for root password

# if [ -z "$SUBDOMAIN" ] || [ -z "$GENERATED_PASSWORD" ] || [ -z "$EMAIL" ] || [ -z "$NAME" ]; then
#   echo "Usage: ./deploy.sh subdomain generated_password email name"
#   exit 1
# fi

# Set a dynamic project name
PROJECT_NAME="mps_deploy_${SUBDOMAIN}"

# Create a temporary docker-compose file with replaced volume names
TEMP_DOCKER_COMPOSE_FILE="docker-compose-${SUBDOMAIN}.yml"
cp docker-compose.yml $TEMP_DOCKER_COMPOSE_FILE
sed -i "s/\${SUBDOMAIN}/${SUBDOMAIN}/g" $TEMP_DOCKER_COMPOSE_FILE

# Start or rebuild services with a custom project name
docker-compose -p $PROJECT_NAME -f $TEMP_DOCKER_COMPOSE_FILE up -d --build

# Wait for database to be ready
echo "Waiting for database to be ready..."
until docker exec ${PROJECT_NAME}-${SUBDOMAIN}_db-1 mysqladmin --user=${MYSQL_USER} --password=${MYSQL_PASSWORD} --host "127.0.0.1" ping --silent; do
    printf "."
    sleep 1
done
echo "Database is ready."

# Generate password hash using PHP's password_hash function.
ENCRYPTED_PASSWORD=${PASSWORDHASH:-$(php -r "echo password_hash('${GENERATED_PASSWORD}', PASSWORD_DEFAULT);")}


# Debug: Print the generated hash to ensure it's captured correctly
echo "Password hash: $PASSWORDHASH"
echo "Generated hash: $ENCRYPTED_PASSWORD"

# Insert user directly into the database
docker exec ${PROJECT_NAME}-${SUBDOMAIN}_db-1 mysql -u${MYSQL_USER} -p${MYSQL_PASSWORD} ${MYSQL_DATABASE} -sse "INSERT INTO wp_users (user_login, user_pass, user_nicename, user_email, user_registered, user_status) VALUES ('${NAME}', '${ENCRYPTED_PASSWORD}', '${NAME}', '${EMAIL}', NOW(), 0);"


# USER_ID=$(docker exec ${PROJECT_NAME}-db-1 mysql -sse "SELECT LAST_INSERT_ID();")
# docker exec ${PROJECT_NAME}-db-1 mysql -u${MYSQL_USER} -p${MYSQL_PASSWORD} ${MYSQL_DATABASE} -sse "INSERT INTO wp_usermeta (user_id, meta_key, meta_value) VALUES (${USER_ID}, 'wp_capabilities', 'a:1:{s:13:\"super_admin\";b:1;}'), (${USER_ID}, 'wp_user_level', '10');"

# Retrieve the user ID of the newly created user
USER_ID=$(docker exec ${PROJECT_NAME}-${SUBDOMAIN}_db-1 mysql -u${MYSQL_USER} -p${MYSQL_PASSWORD} ${MYSQL_DATABASE} -sse "SELECT ID FROM wp_users WHERE user_email='${EMAIL}';")

# Debug: Print the USER_ID to ensure it's captured correctly
echo "New User ID: $USER_ID"

# Check if USER_ID is not empty
if [ -z "$USER_ID" ]; then
    echo "Error: Failed to retrieve USER_ID."
    exit 1
fi

# Properly formatted serialized PHP array for wp_capabilities
WP_CAPABILITIES="a:1:{s:11:\"super_admin\";b:1;}"

# Insert usermeta data into the database
docker exec ${PROJECT_NAME}-${SUBDOMAIN}_db-1 mysql -u${MYSQL_USER} -p${MYSQL_PASSWORD} ${MYSQL_DATABASE} -sse "INSERT INTO wp_usermeta (user_id, meta_key, meta_value) VALUES (${USER_ID}, 'wp_capabilities', '${WP_CAPABILITIES}'), (${USER_ID}, 'wp_user_level', '10'), (${USER_ID}, 'first_name', '${NAME}');"

# Update 'siteurl' and 'home' options in wp_options
docker exec ${PROJECT_NAME}-${SUBDOMAIN}_db-1 mysql -u${MYSQL_USER} -p${MYSQL_PASSWORD} ${MYSQL_DATABASE} -sse "UPDATE wp_options SET option_value='https://${SUBDOMAIN}.scaleerp.com' WHERE option_name='siteurl';"
docker exec ${PROJECT_NAME}-${SUBDOMAIN}_db-1 mysql -u${MYSQL_USER} -p${MYSQL_PASSWORD} ${MYSQL_DATABASE} -sse "UPDATE wp_options SET option_value='https://${SUBDOMAIN}.scaleerp.com' WHERE option_name='home';"

# Insert 'user_limit' into the wp_options table with the value from $USERS, this comes from the API endpoint depending on the limit of users a specific option was paid for
docker exec ${PROJECT_NAME}-${SUBDOMAIN}_db-1 mysql -u${MYSQL_USER} -p${MYSQL_PASSWORD} ${MYSQL_DATABASE} -sse "INSERT INTO wp_options (option_name, option_value, autoload) VALUES ('user_limit', '${USERS}', 'yes');"

# Get the container names by filtering the output of `docker ps` (assuming the naming pattern is consistent)
WP_CONTAINER_NAME=$(docker ps --filter "name=${PROJECT_NAME}-${SUBDOMAIN}_wordpress-1" --format "{{.Names}}")
DB_CONTAINER_NAME=$(docker ps --filter "name=${PROJECT_NAME}-${SUBDOMAIN}_db-1" --format "{{.Names}}")

echo "WP Container Name: $WP_CONTAINER_NAME"
echo "DB Container Name: $DB_CONTAINER_NAME"

# Prepare email content
EMAIL_SUBJECT="Deployment Completed for $SUBDOMAIN.scaleerp.com"
EMAIL_BODY="Deployment for $SUBDOMAIN has been completed successfully.\n\nDetails:\nSubdomain: https://$SUBDOMAIN.scaleerp.com\nNew User ID: $NAME \nUser Password: $GENERATED_PASSWORD \nPlease make sure, to change your password upon loging in to the system!"

# Send email using ssmtp
# echo -e "Subject:$EMAIL_SUBJECT\n$EMAIL_BODY" | mail -s "$EMAIL_SUBJECT" "$RECIPIENT_EMAIL"
# echo "$EMAIL_BODY" | mail -s "$EMAIL_SUBJECT" -a "From: mps@wooshwp.com" $EMAIL
if [ -z "$PASSWORDHASH" ]; then
  echo "$EMAIL_BODY" | mail -s "$EMAIL_SUBJECT" -a "From: mps@wooshwp.com" $EMAIL
else
  echo "passwordHash exists, email will not be sent."
fi

# Optionally, remove the temporary file after use (uncomment the line below)
# rm $TEMP_DOCKER_COMPOSE_FILE
# rm .env # Optionally, remove the .env file after use